﻿using GameAssistantWeb.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IAccountService
    {
        GameAssistantWeb.Models.Identity.ApplicationUser GetUserByName(string UserName);

        GameAssistantWeb.Models.Identity.ApplicationUser GetUserById(int OwnerProjectId);
    }
}
