﻿using GameAssistantWeb.Models.Project;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface ILevelService
    {

        IEnumerable<LevelViewModel> GetLevels(int ProjectId);

        void AddLevel(LevelViewModel model, int UserId);

        LevelViewModel GetLevel(int ProjectId, int LevelId);

        LevelViewModel GetLevel(int LevelId);

        void EditLevel(LevelViewModel model, int UserId);

        void DeleteLevel(int LevelId, int UserId);

        Level GetLevelDTO(int LevelId);
    }
}
