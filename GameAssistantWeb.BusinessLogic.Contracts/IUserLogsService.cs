﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IUserLogsService
    {
        IEnumerable<Models.Logs.User> GetUsers(int ProjectId);

        Models.Logs.User GetUser(int UserId);

        bool DeleteUser(int UserId, int OwnerProjectId);

        IEnumerable<ViewModels.Logs.UserActivity> GetUsersActivity(int ProjectId);

        Models.Logs.User AddUser(Models.Logs.User UserToAdd, int OwnerProjectId);

        Models.Logs.User UpdateUser(Models.Logs.User UserToAdd, int OwnerProjectId);

        Models.Logs.User GetUser(int ProjectId, string SourceUserId);

        IEnumerable<Models.Logs.User> GetUsers();
    }
}
