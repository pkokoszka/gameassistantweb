﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IAchievedAchievementLogsService
    {

        IEnumerable<Models.Logs.AchievedAchievement> GetUserAchievedAchievements(int UserId, int ProjectId);
        
        IEnumerable<Models.Logs.AchievedAchievement> GetUserAchievedAchievements(int UserId);

        IEnumerable<Models.Logs.AchievedAchievement> GetAchievedAchievements();

        IEnumerable<Models.Logs.AchievedAchievement> GetAchievedEvents(int ProjectId);

        AchievedAchievement GetAchievedAchievement(int AchievedAchievementId);

        bool DeleteAchievedAchievement(int AchievedAchievementId, int UserId);

        AchievedAchievement AddAchievedAchievement(AchievedAchievement AchievedAchievementToAdd, int UserId);
    }
}
