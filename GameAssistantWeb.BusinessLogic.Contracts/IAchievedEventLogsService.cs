﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IAchievedEventLogsService
    {

        IEnumerable<Models.Logs.AchievedEvent> GetUserAchievedEvents(int UserId, int ProjectId);

        IEnumerable<Models.Logs.AchievedEvent> GetUserAchievedEvents(int UserId);

        bool DeleteAchievedEvent(int AchievedEventId, int UserId);

        IEnumerable<Models.Logs.AchievedEvent> GetAchievedEvents(int ProjectId);

        IEnumerable<Models.Logs.AchievedEvent> GetAchievedEvents();

        AchievedEvent AddAchievedEvent(Models.Logs.AchievedEvent AchievedEventToAdd, int UserId);
    }
}
