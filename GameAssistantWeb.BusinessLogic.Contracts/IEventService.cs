﻿using GameAssistantWeb.Models.Project;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IEventService
    {

        IEnumerable<EventViewModel> GetEvents(int ProjectId);

        EventViewModel GetEvent(int EventId);

        Event GetEventDTO(int EventId);

        void AddEvent(EventViewModel model, int UserId);

        void EditEvent(EventViewModel model, int UserId);

        void DeleteEvent(int EventId, int UserId);
    }
}
