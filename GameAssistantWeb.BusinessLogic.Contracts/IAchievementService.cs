﻿using GameAssistantWeb.Models.Project;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IAchievementService
    {

        IEnumerable<AchievementViewModel> GetAchievements(int ProjectId);

        AchievementViewModel CreateAchievementViewModel(int ProjectId);

        AchievementViewModel GetAchievement(int AchievementId, int ProjectId);

        AchievementViewModel GetAchievement(int AchievementId);

        void AddAchievement(AchievementViewModel model, int UserId);

        void EditAchievement(AchievementViewModel model, int UserId);

        void DeleteAchievement(int AchievementId, int UserId);

        Models.Project.Achievement GetAchievementDTO(int AchievementId);
    }
}
