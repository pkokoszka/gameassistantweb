﻿using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IProjectService
    {
        IEnumerable<ProjectViewModel> GetProjetsByUserId(int UserId);
        void AddProject(ViewModels.Project.ProjectViewModel model, int UserId);
        ViewModels.Project.ProjectViewModel GetProjectById(int ProjectId, int UserId);
        ViewModels.Project.ProjectViewModel GetProjectById(int ProjectId);
        void DeleteProject(int ProjectId, int UserId);
        void EditProject(ProjectViewModel model, int UserId);
    }
}
