﻿using GameAssistantWeb.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Contracts
{
    public interface IGameMechanicsService
    {
        void CalculateUserPoints(int UserId);
        void CalculateUserLevel(int UserId);
        void CalculateUserGameStatistics(int UserId);

        void CalculateUserRepeatingAchievements(int UserId, int EventId);
    }
}
