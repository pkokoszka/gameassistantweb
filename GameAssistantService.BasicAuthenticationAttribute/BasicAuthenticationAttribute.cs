﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace GameAssistantService.BasicAuthenticationAttribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class BasicAuthenticationAttribute : Attribute, IServiceBehavior, IEndpointBehavior
    {
        private Func<string, string, bool> _validator;

        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters) { }
        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime) { }
        void IEndpointBehavior.Validate(ServiceEndpoint endpoint) { }
        void IServiceBehavior.Validate(ServiceDescription desc, ServiceHostBase host) { }
        void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters) { }

        public BasicAuthenticationAttribute()
        {
            _validator = BasicAuthenticationPasswordValidator.ValidateUser;
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {
            ChannelDispatcher channelDispatcher = endpointDispatcher.ChannelDispatcher;
            if (channelDispatcher != null)
            {
                foreach (EndpointDispatcher ed in channelDispatcher.Endpoints)
                {
                    ed.DispatchRuntime.MessageInspectors.Add(new BasicAuthenticationHttpHeaderInjector { ValidateMethod = _validator });
                }
            }
        }

        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription desc, ServiceHostBase host)
        {
            foreach (ChannelDispatcher cDispatcher in host.ChannelDispatchers)
            {
                foreach (EndpointDispatcher eDispatcher in cDispatcher.Endpoints)
                {
                    eDispatcher.DispatchRuntime.MessageInspectors.Add(new BasicAuthenticationHttpHeaderInjector { ValidateMethod = _validator });
                }
            }
        }
    }
}