﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace GameAssistantService.BasicAuthenticationAttribute
{
    public class BasicAuthenticationHttpHeaderInjector : IDispatchMessageInspector
    {
        public Func<string, string, bool> ValidateMethod { get; set; }
        public void BeforeSendReply(ref Message reply, object correlationState) { }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            object rmp;
            string header = null;

            if (OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out rmp))
            {
                var requestProp = (HttpRequestMessageProperty)rmp;
                header = requestProp.Headers[HttpRequestHeader.Authorization];
            }
            if (!OperationContext.Current.OutgoingMessageProperties.TryGetValue(HttpResponseMessageProperty.Name, out rmp))
            {
                rmp = new HttpResponseMessageProperty();
                OperationContext.Current.OutgoingMessageProperties.Add(HttpResponseMessageProperty.Name, rmp);
            }
            if (header == null)
            {
                // there's no Authorization header, so we respond with a 401 challenge
                var rp = (HttpResponseMessageProperty)rmp;
                rp.Headers[HttpResponseHeader.WwwAuthenticate] = "Basic realm=\"localhost\"";
                rp.StatusCode = HttpStatusCode.Unauthorized;
                rp.SuppressEntityBody = true;
                instanceContext.Abort();
                return null;
            }
            else
            {
                // if we get here we need to validate the username / password
                string decoded = DecodeAuthorizationHeader(header);
                string user, pass;
                GetUsernamePassword(decoded, out user, out pass);

                if (!ValidateMethod(user, pass))
                {
                    // the username and password is not valid, respond with a 403
                    HttpResponseMessageProperty rp = (HttpResponseMessageProperty)rmp;
                    rp.StatusCode = HttpStatusCode.Forbidden;
                    rp.SuppressEntityBody = true;
                    instanceContext.Abort();
                }
            }

            return null;
        }

        public static void GetUsernamePassword(string auth, out string username, out string password)
        {
            username = string.Empty;
            password = string.Empty;

            if (string.IsNullOrWhiteSpace(auth) || !auth.Contains(":")) return;
            string[] parts = auth.Split(':');
            if (parts == null) return;

            if (parts.Length > 0) username = parts[0];
            if (parts.Length > 1) password = parts[1];
        }

        public static string DecodeAuthorizationHeader(string header)
        {
            if (string.IsNullOrWhiteSpace(header) || !header.Contains(" ")) return string.Empty;

            string[] parts = header.Split(' ');
            if (parts != null && parts.Length > 1)
            {
                string decoded = parts[1];
                decoded = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(decoded));
                return decoded;
            }
            return string.Empty;
        }

        public static string GetUserName(string header)
        {

            string decoded = DecodeAuthorizationHeader(header);
            string user, pass;
            GetUsernamePassword(decoded, out user, out pass);

            return user;
        }

    }
}