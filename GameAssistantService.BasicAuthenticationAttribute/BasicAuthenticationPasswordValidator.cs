﻿using GameAssistantWeb.DataAccessLayer.Implementation;
using GameAssistantWeb.Identity;
using GameAssistantWeb.Membership;
using System;

namespace GameAssistantService.BasicAuthenticationAttribute
{
    public class BasicAuthenticationPasswordValidator
    {
        public static bool ValidateUser(string user, string pwd)
        {
            CustomUserStore userStore = new CustomUserStore(GameAssistantContext.Create());
            ApplicationUserManager userManager = new ApplicationUserManager(userStore);

            var userToAuth = userManager.FindAsync(user, pwd);
            if (userToAuth.Result == null)
                return false;
            else
                return true;

        }
    }
}