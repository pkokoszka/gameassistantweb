﻿using GameAssistantService.BasicAuthenticationAttribute;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GameAssistantService.EventsService
{
    [System.ServiceModel.ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [GameAssistantService.BasicAuthenticationAttribute.BasicAuthentication]
    public class AchievedEventsService : IAchievedEventsService
    {

        private IAccountService _accountService;
        private IProjectService _projectService;
        private IUserLogsService _userLogService;
        private IEventService _eventService;
        private IAchievedEventLogsService _achievedEventLogsService;
        private IAchievedAchievementLogsService _achievedAchievementLogsService;


        public AchievedEventsService(IAccountService accountService, IEventService eventService, IProjectService projectService, IUserLogsService userLogService, IAchievedEventLogsService achievedEventLogsService, IAchievedAchievementLogsService achievedAchievementLogsService)
        {
            _eventService = eventService;
            _accountService = accountService;
            _projectService = projectService;
            _userLogService = userLogService;
            _achievedAchievementLogsService = achievedAchievementLogsService;
            _achievedEventLogsService = achievedEventLogsService;
        }

        public AchievedEvent[] GetAllAchievedEvents()
        {
            string name = "";

            if (!GetData(out name))
                return null;

            var owner = _accountService.GetUserByName(name);

            var toReturn = _achievedEventLogsService.GetAchievedEvents().Where(x => x.Project.OwnerId == owner.Id);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedEvent { UserId = x.UserId, ProjectId = x.ProjectId, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId, del = x.del, AchieveDate = x.AchieveDate, AchievedEventId = x.AchievedEventId, EventId = x.EventId });
                foreach (var item in toReturn)
                {
                    item.Event = _eventService.GetEventDTO(item.EventId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedEvent[0];
        }

        public GameAssistantWeb.Models.Logs.AchievedEvent[] GetAllAchievedEventsByProjectId(string projectid)
        {
            string name = "";
            int ProjectId = 0;

            if (!GetData(projectid, out ProjectId, out name))
                return null;

            var owner = _accountService.GetUserByName(name);

            var toReturn = _achievedEventLogsService.GetAchievedEvents(ProjectId).Where(x => x.Project.OwnerId == owner.Id);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedEvent { UserId = x.UserId, ProjectId = x.ProjectId, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId , del = x.del, AchieveDate = x.AchieveDate, AchievedEventId = x.AchievedEventId, EventId = x.EventId });
                foreach (var item in toReturn)
                {
                    item.Event = _eventService.GetEventDTO(item.EventId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedEvent[0];
        }

        public GameAssistantWeb.Models.Logs.AchievedEvent[] GetUserAchievedEvents(string projectid, string sourceuserid)
        {
            string name = "";
            int ProjectId = 0;

            if (!GetData(projectid, out ProjectId,  out name))
                return null;

            var ProjectUser = _userLogService.GetUser(ProjectId, sourceuserid);
            var toReturn = _achievedEventLogsService.GetUserAchievedEvents(ProjectUser.UserId);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedEvent { UserId = x.UserId, ProjectId = x.ProjectId, del = x.del, 
                    AchieveDate = x.AchieveDate, AchievedEventId = x.AchievedEventId, EventId = x.EventId, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId }).ToList();
                foreach (var item in toReturn)
                {
                    item.Event = _eventService.GetEventDTO(item.EventId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedEvent[0];
        }

        public AchievedEvent GetAchievedEvent(string projectid, string achievedeventid)
        {
            string name = "";
            int ProjectId, AchievedEventId = 0;

            if (!GetData(projectid, achievedeventid, out ProjectId, out AchievedEventId, out name))
                return null;
            
            var owner = _accountService.GetUserByName(name);

            var toReturn = _achievedEventLogsService.GetAchievedEvents(ProjectId).FirstOrDefault(x => x.Project.OwnerId == owner.Id && x.AchievedEventId == AchievedEventId);//.Select(x => new AchievedEvent { UserId = x.UserId, ProjectId = x.ProjectId, del = x.del, AchieveDate = x.AchieveDate, AchievedEventId = x.AchievedEventId, EventId = x.EventId });
            if (toReturn != null)
            {
                toReturn.Event = _eventService.GetEventDTO(toReturn.EventId);
                
                return toReturn;
            }
            else
                return new AchievedEvent();
        }

        public GameAssistantWeb.Models.Logs.AchievedEvent AddAchievedEvent(AchievedEvent AchievedEventToAdd)
        {
            string name = "";
            if (!GetData(out name) || AchievedEventToAdd == null)
                return null;

            var user = _accountService.GetUserByName(name);

            var AddedAchievedEvent = _achievedEventLogsService.AddAchievedEvent(AchievedEventToAdd, user.Id);
            AddedAchievedEvent.Event = _eventService.GetEventDTO(AddedAchievedEvent.EventId);


            return AddedAchievedEvent;
        }


        public bool DeleteAchievedEvent(string projectid, string achievedeventid)
        {
            bool deleted = false;
            string name = "";
            int ProjectId, AchievedEventId = 0;

            if (!GetData(projectid, achievedeventid, out ProjectId, out AchievedEventId, out name))
                return deleted;

            var user = _accountService.GetUserByName(name);
            var achievedEventToDelete = GetAchievedEvent(projectid, achievedeventid);

            if (achievedEventToDelete == null)
                return deleted;

            deleted = _achievedEventLogsService.DeleteAchievedEvent(achievedEventToDelete.AchievedEventId, user.Id);
            return deleted;
        }


        public static bool GetData(string projectid, out int ProjectId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || name == "")
            {
                ProjectId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad danych wejsciowych";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(string projectid, string sourceuserid, out int ProjectId, out int SourceUserId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || !Int32.TryParse(sourceuserid, out SourceUserId) || name == "")
            {
                ProjectId = SourceUserId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad danych wejsciowych";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (name == "")
            {
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad autoryzacji";
                return false;
            }
            else
                return true;

        }

    }
}
