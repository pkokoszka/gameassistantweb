﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GameAssistantService.EventsService
{
    [ServiceContract]
    public interface IAchievedEventsService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedEvent[] GetAllAchievedEvents();

        [OperationContract]
        [WebGet(UriTemplate = "/{projectid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedEvent[] GetAllAchievedEventsByProjectId(string projectid);

        [OperationContract] 
        [WebGet(UriTemplate = "/{projectid}/{sourceuserid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedEvent[] GetUserAchievedEvents(string projectid, string sourceuserid);


        [OperationContract]
        [WebInvoke(UriTemplate = "/", Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedEvent AddAchievedEvent(AchievedEvent AchievedEventToAdd);



        [OperationContract]
        [WebInvoke(UriTemplate = "/{projectid}/{achievedeventid}", Method = "DELETE", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool DeleteAchievedEvent(string projectid, string achievedeventid);
    }
}
