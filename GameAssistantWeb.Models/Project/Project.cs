﻿using GameAssistantWeb.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Models.Project
{
    public class Project
    {
        public int ProjectId { get; set; }
        
        public string ProjectName { get; set; }

        public string ProjectShortName { get; set; }
        
        public DateTime CreateDate { get; set; }
        
        public string ProjectDescription { get; set; }
        
        public bool del { get; set; }
        
        public int OwnerId { get; set; }
        
        public virtual ApplicationUser Owner { get; set; }
        
        public virtual ICollection<ProjectComponent> Components { get; set; }
    }
}
