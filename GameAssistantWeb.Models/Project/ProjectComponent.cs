﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Models.Project
{
    public class ProjectComponent
    {
        public int ProjectComponentId { get; set; }

        public string ComponentName { get; set; }

        public DateTime CreateDate { get; set; }
        
        public bool del { get; set; }
        
        public int ProjectId { get; set; }
        
        public virtual Project Project { get; set; }
    }
}
