﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Models.Project
{
    [DataContract]
    public class Achievement
    {
        [DataMember]
        public int AchievementId { get; set; }
        [DataMember]
        public int AchievementType { get; set; } //enums
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int? PointsForAchieve { get; set; }

        public DateTime CreateDate { get; set; }
        public bool del { get; set; }

        public int? EventsIteration { get; set; }

        public int ConnectedEventId { get; set; }
        public virtual Event Event { get; set; }

        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public virtual ICollection<AchievedAchievement> AchievedAchievements { get; set; }
    }
}
