﻿using GameAssistantWeb.Models.Project;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Models.Logs
{
    [DataContract]
    [Table("Logs_User")]
    public class User
    {

        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string SourceUserId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int Points { get; set; }
        [DataMember]
        public int LevelId { get; set; }
        [DataMember]
        public virtual Level CurrentLevel { get; set; }
        [DataMember]
        public string AdditionalData { get; set; }
        [DataMember]
        public DateTime? CreateDate { get; set; }
        public bool del { get; set; }
        [DataMember]
        public int ProjectId { get; set; }


        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }
        [DataMember]
        public virtual ICollection<AchievedEvent> AchievedEvents { get; set; }
        [DataMember]
        public virtual ICollection<AchievedAchievement> AchievedAchievements { get; set; }

    }
}
