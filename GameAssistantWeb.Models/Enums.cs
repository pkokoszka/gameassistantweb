﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Models
{
    public class Enums
    {
        public enum AchievementType
        {
            [Display(Name = "For single event")]
            ForSingleEvent = 1,

            [Display(Name = "For repeating event")]
            ForRepeatingEvent = 2,

            [Display(Name = "For time interval (need to manually implement)")]
            ForTimeInterval = 3,

            [Display(Name = "Custom (need to manually implement)")]
            Custom = 4,
        };

    }


}
