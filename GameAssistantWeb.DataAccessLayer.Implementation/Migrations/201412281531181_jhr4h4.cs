namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jhr4h4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Logs_User", "CreateDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Logs_User", "CreateDate", c => c.DateTime(nullable: false));
        }
    }
}
