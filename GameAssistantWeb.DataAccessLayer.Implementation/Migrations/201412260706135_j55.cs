namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class j55 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Logs_AchievedAchievements", "SourceUserId", c => c.String());
            AlterColumn("dbo.Logs_User", "SourceUserId", c => c.String());
            AlterColumn("dbo.Logs_AchieveEvent", "SourceUserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Logs_AchieveEvent", "SourceUserId", c => c.Int(nullable: false));
            AlterColumn("dbo.Logs_User", "SourceUserId", c => c.Int(nullable: false));
            AlterColumn("dbo.Logs_AchievedAchievements", "SourceUserId", c => c.Int(nullable: false));
        }
    }
}
