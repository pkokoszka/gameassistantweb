namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Logs_User", "ProjectUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Logs_User", "ProjectUserId", c => c.String());
        }
    }
}
