namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class xx : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achievements", "EventsIteration", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Achievements", "EventsIteration");
        }
    }
}
