namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achievements",
                c => new
                    {
                        AchievementId = c.Int(nullable: false, identity: true),
                        AchievementType = c.Int(nullable: false),
                        Title = c.String(),
                        PointsForAchieve = c.Int(),
                        CreateDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                        ConnectedEventId = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AchievementId)
                .ForeignKey("dbo.Events", t => t.ConnectedEventId, cascadeDelete: false)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .Index(t => t.ConnectedEventId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        EventNameId = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        PointsForAchieve = c.Int(nullable: false),
                        MultipleEvent = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(),
                        ProjectShortName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        ProjectDescription = c.String(),
                        del = c.Boolean(nullable: false),
                        OwnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId, cascadeDelete: false)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.ProjectComponents",
                c => new
                    {
                        ProjectComponentId = c.Int(nullable: false, identity: true),
                        ComponentName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectComponentId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Logs_AchievedAchievements",
                c => new
                    {
                        AchievedAchievementId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AchievementId = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        AchieveDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AchievedAchievementId)
                .ForeignKey("dbo.Achievements", t => t.AchievementId, cascadeDelete: false)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .ForeignKey("dbo.Logs_User", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.AchievementId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Logs_User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        SourceUserId = c.Int(nullable: false),
                        ProjectUserId = c.String(),
                        UserName = c.String(),
                        Points = c.Int(nullable: false),
                        LevelId = c.Int(nullable: false),
                        AdditionalData = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Levels", t => t.LevelId, cascadeDelete: false)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .Index(t => t.LevelId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        LevelId = c.Int(nullable: false, identity: true),
                        LevelTitle = c.String(),
                        LowerThreshold = c.Int(nullable: false),
                        UpperThreshold = c.Int(nullable: false),
                        UpperThresholdInf = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LevelId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Logs_AchieveEvent",
                c => new
                    {
                        AchievedEventId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        EventId = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        AchieveDate = c.DateTime(nullable: false),
                        del = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AchievedEventId)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: false)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: false)
                .ForeignKey("dbo.Logs_User", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.EventId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Logs_AchieveEvent", "UserId", "dbo.Logs_User");
            DropForeignKey("dbo.Logs_AchieveEvent", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Logs_AchieveEvent", "EventId", "dbo.Events");
            DropForeignKey("dbo.Logs_AchievedAchievements", "UserId", "dbo.Logs_User");
            DropForeignKey("dbo.Logs_User", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Logs_User", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Levels", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Logs_AchievedAchievements", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Logs_AchievedAchievements", "AchievementId", "dbo.Achievements");
            DropForeignKey("dbo.Achievements", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Achievements", "ConnectedEventId", "dbo.Events");
            DropForeignKey("dbo.Events", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProjectComponents", "ProjectId", "dbo.Projects");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Logs_AchieveEvent", new[] { "ProjectId" });
            DropIndex("dbo.Logs_AchieveEvent", new[] { "EventId" });
            DropIndex("dbo.Logs_AchieveEvent", new[] { "UserId" });
            DropIndex("dbo.Levels", new[] { "ProjectId" });
            DropIndex("dbo.Logs_User", new[] { "ProjectId" });
            DropIndex("dbo.Logs_User", new[] { "LevelId" });
            DropIndex("dbo.Logs_AchievedAchievements", new[] { "ProjectId" });
            DropIndex("dbo.Logs_AchievedAchievements", new[] { "AchievementId" });
            DropIndex("dbo.Logs_AchievedAchievements", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ProjectComponents", new[] { "ProjectId" });
            DropIndex("dbo.Projects", new[] { "OwnerId" });
            DropIndex("dbo.Events", new[] { "ProjectId" });
            DropIndex("dbo.Achievements", new[] { "ProjectId" });
            DropIndex("dbo.Achievements", new[] { "ConnectedEventId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Logs_AchieveEvent");
            DropTable("dbo.Levels");
            DropTable("dbo.Logs_User");
            DropTable("dbo.Logs_AchievedAchievements");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ProjectComponents");
            DropTable("dbo.Projects");
            DropTable("dbo.Events");
            DropTable("dbo.Achievements");
        }
    }
}
