namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cq1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs_AchievedAchievements", "PointsAchieved", c => c.Int(nullable: false));
            AddColumn("dbo.Logs_AchieveEvent", "PointsAchieved", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs_AchieveEvent", "PointsAchieved");
            DropColumn("dbo.Logs_AchievedAchievements", "PointsAchieved");
        }
    }
}
