namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class chfsd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs_AchievedAchievements", "SourceUserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs_AchievedAchievements", "SourceUserId");
        }
    }
}
