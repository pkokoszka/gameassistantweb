namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dadqq : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs_AchieveEvent", "SourceUserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs_AchieveEvent", "SourceUserId");
        }
    }
}
