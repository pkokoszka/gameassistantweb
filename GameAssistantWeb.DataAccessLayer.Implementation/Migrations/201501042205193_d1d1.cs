namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class d1d1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Logs_AchievedAchievements", "AchieveDate", c => c.DateTime());
            AlterColumn("dbo.Logs_AchieveEvent", "AchieveDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Logs_AchieveEvent", "AchieveDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Logs_AchievedAchievements", "AchieveDate", c => c.DateTime(nullable: false));
        }
    }
}
