﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.DataAccessLayer.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        IGameAssistantContext tcContext;

        private DbContext dbContext;
        private DbContext DbContext
        {
            get { return dbContext ?? (dbContext = tcContext as DbContext); }
        }


        public UnitOfWork(IGameAssistantContext cc)
        {
            this.tcContext = new GameAssistantContext();//cc;
        }

        public DbSet<T> GetDbSet<T>()
            where T : class
        {
            return DbContext.Set<T>();
        }

        public void Commit()
        {
            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                //_logger.ErrorFormat(ex, "Error occurred while Commit method: {1} {2}", ex.Message, ex.Source);
            }

        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
