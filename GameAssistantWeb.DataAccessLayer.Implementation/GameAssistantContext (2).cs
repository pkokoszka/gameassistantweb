﻿using GameAssistant.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.DataAccessLayer.Implementation
{
    public class GameAssistantContext : IdentityDbContext<ApplicationUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>, IGameAssistantContext
    {
        public GameAssistantContext()
            : base("DefaultConnection")
        {

        }

        public static GameAssistantContext Create()
        {
            return new GameAssistantContext();
        }

        public DbSet<User> ProjectUsers { get; set; }
        public DbSet<AchievedEvent> AchievedEvents { get; set; }
        public DbSet<AchievedAchievement> AchievedAchievements { get; set; }
        public DbSet<Achievement> Achiements { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectComponent> ProjectComponents { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //relationship between ProjectComponent entity and Project
            modelBuilder.Entity<ProjectComponent>().HasRequired<Project>(s => s.Project)
            .WithMany(s => s.Components).HasForeignKey(s => s.ProjectId);

            //relationship between Project and ApplicationUser
            modelBuilder.Entity<Project>().HasRequired(u => u.Owner).WithMany().HasForeignKey(h => h.OwnerId);

            //Achievement => Event
            modelBuilder.Entity<Achievement>().HasRequired(u => u.Event).WithMany().HasForeignKey(x => x.ConnectedEventId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
