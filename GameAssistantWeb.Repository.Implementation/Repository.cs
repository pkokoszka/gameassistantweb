﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Repository.Implementation
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly DbSet<T> dbSet;

        public Repository(IUnitOfWork unitOfWork)
        {
            var efUnitOfWork = unitOfWork;// as UnitOfWork;

            if (efUnitOfWork == null)
            {
                //_logger.Error("Error occurred in Repository constructor", new Exception("Must be UnitOfWork"));
                throw new Exception("Must be UnitOfWork");
            }
            dbSet = efUnitOfWork.GetDbSet<T>();
        }

        public void Add(T item)
        {
            try
            {
                dbSet.Add(item);
            }
            catch (Exception ex)
            {
                //_logger.Error("Error occurred in Add method", ex);
            }

        }

        public void Remove(T item)
        {
            dbSet.Remove(item);
        }

        public IQueryable<T> Query()
        {
            try
            {
                return dbSet;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error occurred in Query method", ex);
                return null;
            }

        }

        public T GetEntityById(int itemId)
        {
            return dbSet.Find(itemId);
        }
    }
}
