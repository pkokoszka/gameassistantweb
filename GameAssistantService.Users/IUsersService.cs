﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GameAssistantService.UsersService
{
    //,
    [ServiceContract]
    public interface IUsersService {
        [OperationContract]
        [WebGet(UriTemplate = "/", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        User[] GetAllUsers(); 

        [OperationContract]
        [WebGet(UriTemplate = "/{projectid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        User[] GetAllUsersByProjectId(string projectid); 

        [OperationContract]
        [WebGet(UriTemplate = "/{projectid}/{sourceuserid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        User GetUser(string projectid, string sourceuserid); 

        [OperationContract]
        [WebInvoke(UriTemplate = "/", Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        User AddUser(User UserToAdd);

        [OperationContract]
        [WebInvoke(UriTemplate = "/", Method = "PUT", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        User UpdateUser(User UserToUpdate);

        [OperationContract]
        [WebInvoke(UriTemplate = "/{projectid}/{sourceuserid}", Method = "DELETE", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool DeleteUser(string projectid, string sourceuserid);
    }

}
