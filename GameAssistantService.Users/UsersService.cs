﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using GameAssistantWeb.Models.Logs;
using System.Text;
using GameAssistantWeb.BusinessLogic.Contracts;
using System.Collections;
using System.Threading;
using GameAssistantService.BasicAuthenticationAttribute;
using GameAssistantWeb.ViewModels.Project;

namespace GameAssistantService.UsersService
{


    [System.ServiceModel.ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [GameAssistantService.BasicAuthenticationAttribute.BasicAuthentication]
    public class UsersService : IUsersService
    {
        private IAccountService _accountService;
        private IProjectService _projectService;
        private IUserLogsService _userLogService;
        private ILevelService _levelService;
        private IAchievedEventLogsService _achievedEventLogsService;
        private IAchievedAchievementLogsService _achievedAchievementLogsService;


        public UsersService(IAccountService accountService, ILevelService levelService, IProjectService projectService, IUserLogsService userLogService, IAchievedEventLogsService achievedEventLogsService, IAchievedAchievementLogsService achievedAchievementLogsService)
        {
            _levelService = levelService;
            _accountService = accountService;
            _projectService = projectService;
            _userLogService = userLogService;
            _achievedAchievementLogsService = achievedAchievementLogsService;
            _achievedEventLogsService = achievedEventLogsService;
        }

        public User[] GetAllUsers()
        {
            string name = "";
            if (!GetData(out name))
                return null;

            var user = _accountService.GetUserByName(name);

            var users = _userLogService.GetUsers().Where(x => x.Project.OwnerId == user.Id).Select(x => new User
            {
                CreateDate = x.CreateDate,
                ProjectId = x.ProjectId,
                SourceUserId = x.SourceUserId,
                UserId = x.UserId,
                UserName = x.UserName,
                Points = x.Points,
                LevelId = x.LevelId,
                del = x.del,
                AdditionalData = x.AdditionalData
            }).OrderByDescending(x => x.Points).ToArray();

            foreach (var item in users)
            {
                item.AchievedAchievements = _achievedAchievementLogsService.GetUserAchievedAchievements(item.UserId).ToList();// as ICollection<AchievedAchievement>;
                item.AchievedEvents = _achievedEventLogsService.GetUserAchievedEvents(item.UserId).ToList();
                item.CurrentLevel = _levelService.GetLevelDTO(item.LevelId);
            }
            return users;

        }

        public User[] GetAllUsersByProjectId(string projectid)
        {
            string name = "";
            int ProjectId = 0;
            if (!GetData(projectid, out ProjectId, out name))
                return null;

            var user = _accountService.GetUserByName(name);

            var users = _userLogService.GetUsers(ProjectId).Where(x => x.Project.OwnerId == user.Id).Select(x => new User
            {
                CreateDate = x.CreateDate,
                ProjectId = x.ProjectId,
                SourceUserId = x.SourceUserId,
                UserId = x.UserId,
                UserName = x.UserName,
                Points = x.Points,
                LevelId = x.LevelId,
                del = x.del,
                AdditionalData = x.AdditionalData
            }).OrderByDescending(x => x.Points).ToArray();

            foreach (var item in users)
            {
                item.AchievedAchievements = _achievedAchievementLogsService.GetUserAchievedAchievements(item.UserId, ProjectId).ToList(); //as ICollection<AchievedAchievement>;
                item.AchievedEvents = _achievedEventLogsService.GetUserAchievedEvents(item.UserId, ProjectId).ToList(); //as ICollection<AchievedEvent>;
                item.CurrentLevel = _levelService.GetLevelDTO(item.LevelId);
            }
            return users;
        }

        public User GetUser(string projectid, string sourceuserid)
        {
            string name = "";
            int ProjectId = 0;

            if (!GetData(projectid, out ProjectId, out name))
                return null;

            var user = _accountService.GetUserByName(name);

            var GetUser = _userLogService.GetUser(ProjectId, sourceuserid);
            if (GetUser == null)
                return new User();
            else
            {
                GetUser = new User {

                    CreateDate = GetUser.CreateDate,
                    ProjectId = GetUser.ProjectId,
                    SourceUserId = GetUser.SourceUserId,
                    UserId = GetUser.UserId,
                    UserName = GetUser.UserName,
                    Points = GetUser.Points,
                    LevelId = GetUser.LevelId,
                    del = GetUser.del,
                    AdditionalData = GetUser.AdditionalData
                };
            }


            GetUser.AchievedAchievements = _achievedAchievementLogsService.GetUserAchievedAchievements(GetUser.UserId).ToList();// as ICollection<AchievedAchievement>;
            GetUser.AchievedEvents = _achievedEventLogsService.GetUserAchievedEvents(GetUser.UserId).ToList();
            GetUser.CurrentLevel = _levelService.GetLevelDTO(GetUser.LevelId);

            return GetUser;
        }

        public User AddUser(User UserToAdd)
        {
            string name = "";
            if (!GetData(out name) || UserToAdd == null)
                return null; 

            var user = _accountService.GetUserByName(name);
            var AddedUser = _userLogService.AddUser(UserToAdd, user.Id);

            AddedUser = new User
            {

                CreateDate = AddedUser.CreateDate,
                ProjectId = AddedUser.ProjectId,
                SourceUserId = AddedUser.SourceUserId,
                UserId = AddedUser.UserId,
                UserName = AddedUser.UserName,
                Points = AddedUser.Points,
                LevelId = AddedUser.LevelId,
                del = AddedUser.del,
                AdditionalData = AddedUser.AdditionalData
            };

            AddedUser.AchievedAchievements = _achievedAchievementLogsService.GetUserAchievedAchievements(AddedUser.UserId) as ICollection<AchievedAchievement>;
            AddedUser.AchievedEvents = _achievedEventLogsService.GetUserAchievedEvents(AddedUser.UserId) as ICollection<AchievedEvent>;
            AddedUser.CurrentLevel = _levelService.GetLevelDTO(AddedUser.LevelId);
            
            return AddedUser;
        }

        public User UpdateUser(User UserToUpdate)
        {
            string name = "";
            if (!GetData(out name) || UserToUpdate.ProjectId == 0)
                return null;

            var user = _accountService.GetUserByName(name);

            var UpdatedUser = _userLogService.UpdateUser(UserToUpdate, user.Id);
            UpdatedUser = new User
            {

                CreateDate = UpdatedUser.CreateDate,
                ProjectId = UpdatedUser.ProjectId,
                SourceUserId = UpdatedUser.SourceUserId,
                UserId = UpdatedUser.UserId,
                UserName = UpdatedUser.UserName,
                Points = UpdatedUser.Points,
                LevelId = UpdatedUser.LevelId,
                del = UpdatedUser.del,
                AdditionalData = UpdatedUser.AdditionalData
            };


            UpdatedUser.AchievedAchievements = _achievedAchievementLogsService.GetUserAchievedAchievements(UpdatedUser.UserId) as ICollection<AchievedAchievement>;
            UpdatedUser.AchievedEvents = _achievedEventLogsService.GetUserAchievedEvents(UpdatedUser.UserId) as ICollection<AchievedEvent>;
            UpdatedUser.CurrentLevel = _levelService.GetLevelDTO(UpdatedUser.LevelId);

            return UpdatedUser;
        }

        public bool DeleteUser(string projectid, string sourceuserid)
        {
            bool deleted = false;
            string name = "";
            int ProjectId, SourceUserId = 0;

            if (!GetData(projectid, sourceuserid, out ProjectId, out SourceUserId, out name))
                return deleted;
            var user = _accountService.GetUserByName(name);
            var userToDelete = GetUser(projectid, sourceuserid);

            if (userToDelete == null)
                return deleted;

            deleted = _userLogService.DeleteUser(userToDelete.UserId, user.Id);
            return deleted;
        }

        public static bool GetData(string projectid, string sourceuserid, out int ProjectId, out int SourceUserId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || !Int32.TryParse(sourceuserid, out SourceUserId) || name == "")
            {
                ProjectId = SourceUserId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad autoryzacji";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(string projectid, out int ProjectId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || name == "")
            {
                ProjectId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad autoryzacji";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (name == "")
            {
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad autoryzacji";
                return false;
            }
            else
                return true;

        }
    }
}
