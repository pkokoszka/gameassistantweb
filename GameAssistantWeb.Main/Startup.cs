﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameAssistantWeb.Startup))]
namespace GameAssistantWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
