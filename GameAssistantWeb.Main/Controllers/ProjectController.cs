﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameAssistantWeb.ViewModels.Project;

namespace GameAssistantWeb.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IAccountService _accountService;
        private readonly IProjectService _projectService;
        private readonly ILevelService _levelService;
        private readonly IEventService _eventService;
        private readonly IAchievementService _achievementService;

        public ProjectController(IUnitOfWork uow, IRepository<ApplicationUser> applicationUserRepository, IAchievementService achievementService, IEventService eventService, ILevelService levelService, IProjectService projectService, IAccountService accountService)
        {
            _uow = uow;
            _accountService = accountService;
            _achievementService = achievementService;
            _projectService = projectService;
            _levelService = levelService;
            _eventService = eventService;
        }


        [HttpGet]
        public ActionResult Index(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            return View();
        }


        [HttpGet]
        public ActionResult Levels(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<LevelViewModel> levels = _levelService.GetLevels(ProjectId);

            return View(levels);
        }

        [HttpGet]
        public PartialViewResult _AddLevelPartial(int ProjectId)
        {
            LevelViewModel model = new LevelViewModel { ProjectId = ProjectId };
            return PartialView("_AddOrEditLevelPartial", model);
        }

        [HttpGet]
        public PartialViewResult _EditLevelPartial(int ProjectId, int LevelId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            LevelViewModel model = _levelService.GetLevel(LevelId);

            if (model.Project.OwnerId != user.Id)
                model = new LevelViewModel();

            return PartialView("_AddOrEditLevelPartial", model);
        }

        [HttpPost]
        public ActionResult AddOrEditLevel(LevelViewModel model)
        {
            if (ShouldRedirect(model.ProjectId))
                return RedirectToAction("Index", "Home");

            var user = _accountService.GetUserByName(User.Identity.Name);

            if (model.LevelId == 0){
                _levelService.AddLevel(model, user.Id);
                TempData["message"] = "Level successfully added";
            }
            else { 
                _levelService.EditLevel(model, user.Id);
                TempData["message"] = "Level successfully edited";
            }

            return RedirectToAction("Levels", new { ProjectId = model.ProjectId });
        }

        [HttpGet]
        public PartialViewResult _DeleteLevelPartial(int LevelId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            LevelViewModel model = _levelService.GetLevel(LevelId);

            if (model.Project.OwnerId != user.Id)
                model = new LevelViewModel();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult DeleteLevel(LevelViewModel model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            _levelService.DeleteLevel(model.LevelId, user.Id);

            TempData["message"] = "Level successfully deleted";
            return RedirectToAction("Levels", new { ProjectId = model.ProjectId });
        }








        [HttpGet]
        public ActionResult Events(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<EventViewModel> events = _eventService.GetEvents(ProjectId);

            return View(events);
        }

        [HttpGet]
        public PartialViewResult _AddEventPartial(int ProjectId)
        {
            EventViewModel model = new EventViewModel { ProjectId = ProjectId };
            return PartialView("_AddOrEditEventPartial", model);
        }

        [HttpGet]
        public PartialViewResult _EditEventPartial(int ProjectId, int EventId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            EventViewModel model = _eventService.GetEvent(EventId);

            if (model.Project.OwnerId != user.Id)
                model = new EventViewModel();

            return PartialView("_AddOrEditEventPartial", model);
        }

        [HttpPost]
        public ActionResult AddOrEditEvent(EventViewModel model)
        {
            if (ShouldRedirect(model.ProjectId))
                return RedirectToAction("Index", "Home");

            var user = _accountService.GetUserByName(User.Identity.Name);

            if (model.EventId == 0)
            {
                _eventService.AddEvent(model, user.Id);
                TempData["message"] = "Event successfully added";
            }
            else
            {
                _eventService.EditEvent(model, user.Id);
                TempData["message"] = "Event successfully edited";
            }

            return RedirectToAction("Events", new { ProjectId = model.ProjectId });
        }

        [HttpGet]
        public PartialViewResult _DeleteEventPartial(int EventId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            EventViewModel model = _eventService.GetEvent(EventId);

            if (model.Project.OwnerId != user.Id)
                model = new EventViewModel();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult DeleteEvent(EventViewModel model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            _eventService.DeleteEvent(model.EventId, user.Id);

            TempData["message"] = "Event successfully deleted";
            return RedirectToAction("Events", new { ProjectId = model.ProjectId });
        }





        [HttpGet]
        public ActionResult Achievements(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<AchievementViewModel> events = _achievementService.GetAchievements(ProjectId);

            return View(events);
        }


        [HttpGet]
        public PartialViewResult _AddAchievementPartial(int ProjectId)
        {
            AchievementViewModel model = _achievementService.CreateAchievementViewModel(ProjectId);
            return PartialView("_AddOrEditAchievementPartial", model);
        }


        [HttpGet]
        public PartialViewResult _EditAchievementPartial(int ProjectId, int AchievementId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            AchievementViewModel model = _achievementService.GetAchievement(AchievementId, ProjectId);

            if (model.Project.OwnerId != user.Id)
                model = _achievementService.CreateAchievementViewModel(ProjectId);

            return PartialView("_AddOrEditAchievementPartial", model);
        }


        [HttpPost]
        public ActionResult AddOrEditAchievement(AchievementViewModel model)
        {
            if (ShouldRedirect(model.ProjectId))
                return RedirectToAction("Index", "Home");

            var user = _accountService.GetUserByName(User.Identity.Name);

            if (model.AchievementId == 0)
            {
                _achievementService.AddAchievement(model, user.Id);
                TempData["message"] = "Event successfully added";
            }
            else
            {
                _achievementService.EditAchievement(model, user.Id);
                TempData["message"] = "Event successfully edited";
            }

            return RedirectToAction("Achievements", new { ProjectId = model.ProjectId });
        }

        [HttpGet]
        public PartialViewResult _DeleteAchievementPartial(int AchievementId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            AchievementViewModel model = _achievementService.GetAchievement(AchievementId);

            if (model.Project.OwnerId != user.Id)
                model = new AchievementViewModel();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult DeleteAchievement(AchievementViewModel model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            _achievementService.DeleteAchievement(model.AchievementId, user.Id);

            TempData["message"] = "Achievement successfully deleted";
            return RedirectToAction("Achievements", new { ProjectId = model.ProjectId });
        }

        public bool ShouldRedirect(int ProjectId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            var project = _projectService.GetProjectById(ProjectId, user.Id);
            if (project == null)
                return true;
            else
            {
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.ProjectId = project.ProjectId;
                return false;
            }
        }
    }
}