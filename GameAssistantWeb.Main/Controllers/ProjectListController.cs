﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.ViewModels.Project;

namespace GameAssistantWeb.Controllers
{
    [Authorize]
    public class ProjectListController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IAccountService _accountService;
        private readonly IProjectService _projectService;

        public ProjectListController(IUnitOfWork uow, IRepository<ApplicationUser> applicationUserRepository, IProjectService projectService, IAccountService accountService)
        {
            _uow = uow;
            _accountService = accountService;
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            IEnumerable<ProjectViewModel> ProjectList = _projectService.GetProjetsByUserId(user.Id);


            return View(ProjectList);
        }

        [HttpGet]
        public PartialViewResult _AddProjectPartial()
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            ProjectViewModel model = new ProjectViewModel();

            return PartialView("_AddOrEditProjectPartial", model);
        }

        [HttpGet]
        public PartialViewResult _EditProjectPartial(int ProjectId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            ProjectViewModel model = _projectService.GetProjectById(ProjectId, user.Id);

            return PartialView("_AddOrEditProjectPartial", model);
        }

        [HttpPost]
        public ActionResult AddOrEditProject(ProjectViewModel model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);

            if (model.ProjectId == 0)
                _projectService.AddProject(model, user.Id);
            else
                _projectService.EditProject(model, user.Id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public PartialViewResult _DeleteProjectPartial(int ProjectId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            ProjectViewModel projectToDelete = _projectService.GetProjectById(ProjectId, user.Id);
            return PartialView(projectToDelete);
        }

        [HttpPost]
        public ActionResult DeleteProject(ProjectViewModel model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            _projectService.DeleteProject(model.ProjectId, user.Id);

            return RedirectToAction("Index");
        }
    }
}