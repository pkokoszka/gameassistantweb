﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameAssistantWeb.ViewModels.Project;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.ViewModels.Logs;

namespace GameAssistantWeb.Controllers
{
    public class ProjectStatisticController : Controller
    {

        private readonly IUnitOfWork _uow;
        private readonly IAccountService _accountService;
        private readonly IEventService _eventService;
        private readonly IAchievementService _achievementService;
        private readonly IProjectService _projectService;
        private readonly IUserLogsService _userLogsService;

        public ProjectStatisticController(IUnitOfWork uow, IUserLogsService userLogsService, IAchievementService achievementService, IEventService eventService, IProjectService projectService, IAccountService accountService)
        {
            _uow = uow;
            _accountService = accountService;
            _userLogsService = userLogsService;
            _achievementService = achievementService;
            _eventService = eventService;
            _projectService = projectService;
        }


        [HttpGet]
        public ActionResult Index(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<User> usersStatistic = _userLogsService.GetUsers(ProjectId);
            return View(usersStatistic);
        }


        [HttpPost]
        public ActionResult Index(int ProjectId, DateTime? fromDateTimePicker, DateTime? toDateTimePicker)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<User> usersStatistic = _userLogsService.GetUsers(ProjectId);
            if (fromDateTimePicker.HasValue && toDateTimePicker.HasValue)
            {
                foreach (var user in usersStatistic)
                {
                    user.AchievedEvents = user.AchievedEvents.Where(x => x.AchieveDate > fromDateTimePicker && x.AchieveDate < toDateTimePicker).ToList();
                    user.AchievedAchievements = user.AchievedAchievements.Where(x => x.AchieveDate > fromDateTimePicker && x.AchieveDate < toDateTimePicker).ToList();
                    user.Points = 0;
                    foreach (var item in user.AchievedAchievements)
                    {
                        user.Points = user.Points + item.PointsAchieved;
                    }
                    foreach (var item in user.AchievedEvents)
                    {
                        user.Points = user.Points + item.PointsAchieved;
                    }
                }
            }
            return View(usersStatistic);
        }


        public ActionResult Users(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<User> usersStatistic = _userLogsService.GetUsers(ProjectId);
            return View(usersStatistic);
        }

        [HttpGet]
        public ActionResult Events(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<EventViewModel> events = _eventService.GetEvents(ProjectId);

            return View(events);
        }

        [HttpGet]
        public ActionResult Achievements(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<AchievementViewModel> events = _achievementService.GetAchievements(ProjectId);

            return View(events);
        }

        [HttpGet]
        public ActionResult Activity(int ProjectId)
        {
            if (ShouldRedirect(ProjectId))
                return RedirectToAction("Index", "Home");

            IEnumerable<UserActivity> events = _userLogsService.GetUsersActivity(ProjectId);

            return View(events);
        }


        [HttpGet]
        public PartialViewResult _DeleteUserPartial(int UserId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            User model = _userLogsService.GetUser(UserId);

            if (model.Project.OwnerId != user.Id)
                model = new User();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult DeleteUser(User model)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            _userLogsService.DeleteUser(model.UserId, user.Id);

            TempData["message"] = "User successfully deleted";
            return RedirectToAction("Users", new { ProjectId = model.ProjectId });
        }



        [HttpGet]
        public PartialViewResult _AdditionalUserDataPartial(int UserId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            User model = _userLogsService.GetUser(UserId);

            if (model.Project.OwnerId != user.Id)
                model = new User();

            return PartialView(model);
        }



        public bool ShouldRedirect(int ProjectId)
        {
            var user = _accountService.GetUserByName(User.Identity.Name);
            var project = _projectService.GetProjectById(ProjectId, user.Id);
            if (project == null)
                return true;
            else
            {
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.ProjectId = project.ProjectId;
                return false;
            }
        }
    }
}