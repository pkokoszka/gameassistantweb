﻿using AutoMapper;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameAssistantWeb.App_Start
{
    public static class MapperConfig
    {
        public static void RegisterMaps()
        {

            Mapper.CreateMap<Project, ProjectViewModel>();
            Mapper.CreateMap<ProjectViewModel, Project>();

            Mapper.CreateMap<Level, LevelViewModel>();
            Mapper.CreateMap<LevelViewModel, Level>();

            Mapper.CreateMap<Event, EventViewModel>();
            Mapper.CreateMap<EventViewModel, Event>();

            Mapper.CreateMap<Achievement, AchievementViewModel>().ForMember(x => x.Type, opts => opts.MapFrom(src => src.AchievementType));
            Mapper.CreateMap<AchievementViewModel, Achievement>();
        }

    }
}