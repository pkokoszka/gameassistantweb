﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using GameAssistantWeb.Identity;
using GameAssistantWeb.Membership;
using GameAssistantWeb.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameAssistantWeb.Installers
{
    public class ManagerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
             .For(typeof(ApplicationUserManager))
             .ImplementedBy(typeof(ApplicationUserManager))
             .LifestylePerWebRequest());

            container.Register(Component
             .For(typeof(IUserStore<ApplicationUser, int>))
             .ImplementedBy(typeof(CustomUserStore))
             .LifestylePerWebRequest());

            //container.Register(Classes.FromAssemblyNamed("GameAssistantWeb.Repository.Implementation")
            //               .Where(Component.IsInSameNamespaceAs(typeof(Repository<>)))
            //                .WithService.DefaultInterfaces()
            //                .LifestylePerWebRequest());
        }
    }
}