﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.DataAccessLayer.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameAssistantWeb.Installers
{
    public class DataAccessLayerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(typeof(IGameAssistantContext))
                    .ImplementedBy(typeof(GameAssistantContext)).LifestylePerWebRequest());

            container.Register(
                Component.For(typeof(IUnitOfWork))
                    .ImplementedBy(typeof(UnitOfWork)).LifestylePerWebRequest());
        }
    }
}