﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameAssistantWeb.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
             .For(typeof(IRepository<>))
             .ImplementedBy(typeof(Repository<>))
             .LifestylePerWebRequest());

            //container.Register(Classes.FromAssemblyNamed("GameAssistantWeb.Repository.Implementation")
            //               .Where(Component.IsInSameNamespaceAs(typeof(Repository<>)))
            //                .WithService.DefaultInterfaces()
            //                .LifestylePerWebRequest());
        }
    }
}