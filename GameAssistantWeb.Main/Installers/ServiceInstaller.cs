﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using GameAssistantWeb.BusinessLogic.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameAssistantWeb.Installers
{
    public class ServiceInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("GameAssistantWeb.BusinessLogic.Implementation")
                            .Where(Component.IsInSameNamespaceAs<AccountService>())
                            .WithService.DefaultInterfaces()
                            .LifestyleTransient());

            //container.Register(Classes.FromAssemblyNamed("GameAssistantWeb.Repository.Implementation")
            //               .Where(Component.IsInSameNamespaceAs(typeof(Repository<>)))
            //                .WithService.DefaultInterfaces()
            //                .LifestylePerWebRequest());
        }
    }
}