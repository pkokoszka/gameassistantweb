﻿using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Rest;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using GameAssistantService.AchievementsService;
using GameAssistantService.EventsService;
using GameAssistantService.UsersService;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.BusinessLogic.Implementation;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.DataAccessLayer.Implementation;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace GameAssistantService.Main
{
    public class Global : System.Web.HttpApplication
    {
        static IWindsorContainer _container { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {

            _container = new WindsorContainer();

            _container.AddFacility<WcfFacility>().Register
                (
                     Component.For<IUsersService>().ImplementedBy<GameAssistantService.UsersService.UsersService>().Named("Users").LifestylePerWebRequest(),
                     Component.For<IAchievedEventsService>().ImplementedBy<AchievedEventsService>().Named("Events").LifestylePerWebRequest(),
                     Component.For<IAchievedAchievementsService>().ImplementedBy<GameAssistantService.AchievementsService.AchievedAchievementsService>().Named("Achievements").LifestylePerWebRequest()
                );

            _container.Register(Classes.FromAssemblyNamed("GameAssistantWeb.BusinessLogic.Implementation").Where(Component.IsInSameNamespaceAs<AccountService>()).WithService.DefaultInterfaces().LifestylePerWebRequest());
            _container.Register(Component.For(typeof(IGameAssistantContext)).ImplementedBy(typeof(GameAssistantContext)).LifestylePerWebRequest());
            _container.Register(Component.For(typeof(IUnitOfWork)).ImplementedBy(typeof(UnitOfWork)).LifestylePerWebRequest());
            _container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestylePerWebRequest());

            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            // Edit the base address of MyService by replacing the "MyService" string below
            RouteTable.Routes.Add(new ServiceRoute("Users", new WindsorServiceHostFactory<RestServiceModel>(), typeof(IUsersService)));
            RouteTable.Routes.Add(new ServiceRoute("Events", new WindsorServiceHostFactory<RestServiceModel>(), typeof(IAchievedEventsService)));
            RouteTable.Routes.Add(new ServiceRoute("Achievements", new WindsorServiceHostFactory<RestServiceModel>(), typeof(IAchievedAchievementsService)));
        }


        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }
    }
}