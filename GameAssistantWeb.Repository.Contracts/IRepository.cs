﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Repository.Contracts
{
    public interface IRepository<T>
    {
        T GetEntityById(int itemId);
        void Add(T item);
        void Remove(T item);
        IQueryable<T> Query();
    }
}
