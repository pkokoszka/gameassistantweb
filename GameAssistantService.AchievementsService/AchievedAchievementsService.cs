﻿using GameAssistantService.BasicAuthenticationAttribute;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GameAssistantService.AchievementsService
{
    [System.ServiceModel.ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [GameAssistantService.BasicAuthenticationAttribute.BasicAuthentication]
    public class AchievedAchievementsService : IAchievedAchievementsService
    {
        private IAccountService _accountService;
        private IProjectService _projectService;
        private IUserLogsService _userLogService;
        private IAchievementService _achievementService;
        private IAchievedEventLogsService _achievedEventLogsService;
        private IAchievedAchievementLogsService _achievedAchievementLogsService;


        public AchievedAchievementsService(IAccountService accountService, IAchievementService achievementService, IProjectService projectService, IUserLogsService userLogService, IAchievedEventLogsService achievedEventLogsService, IAchievedAchievementLogsService achievedAchievementLogsService)
        {
            _achievementService = achievementService;
            _accountService = accountService;
            _projectService = projectService;
            _userLogService = userLogService;
            _achievedAchievementLogsService = achievedAchievementLogsService;
            _achievedEventLogsService = achievedEventLogsService;
        }

        public GameAssistantWeb.Models.Logs.AchievedAchievement[] GetAllAchievedAchievements()
        {
            string name = "";

            if (!GetData(out name))
                return null;

            var owner = _accountService.GetUserByName(name);

            var toReturn = _achievedAchievementLogsService.GetAchievedAchievements().Where(x => x.Project.OwnerId == owner.Id);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedAchievement { UserId = x.UserId, ProjectId = x.ProjectId, del = x.del, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId, AchieveDate = x.AchieveDate, AchievedAchievementId = x.AchievedAchievementId, Achievement = x.Achievement, AchievementId = x.AchievementId });
                foreach (var item in toReturn)
                {
                    item.Achievement = _achievementService.GetAchievementDTO(item.AchievementId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedAchievement[0];
        }

        public GameAssistantWeb.Models.Logs.AchievedAchievement[] GetAllAchievedAchievementsByProjectId(string projectid)
        {
            string name = "";
            int ProjectId = 0;

            if (!GetData(projectid, out ProjectId, out name))
                return null;

            var owner = _accountService.GetUserByName(name);

            var toReturn = _achievedAchievementLogsService.GetAchievedEvents(ProjectId).Where(x => x.Project.OwnerId == owner.Id);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedAchievement { UserId = x.UserId, ProjectId = x.ProjectId, del = x.del, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId, AchieveDate = x.AchieveDate, AchievedAchievementId = x.AchievedAchievementId, Achievement = x.Achievement, AchievementId = x.AchievementId });
                foreach (var item in toReturn)
                {
                    item.Achievement = _achievementService.GetAchievementDTO(item.AchievementId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedAchievement[0];
        }

        public GameAssistantWeb.Models.Logs.AchievedAchievement[] GetUserAchievedAchievements(string projectid, string sourceuserid)
        {
            string name = "";
            int ProjectId = 0;

            if (!GetData(projectid, out ProjectId,  out name))
                return null;

            var ProjectUser = _userLogService.GetUser(ProjectId, sourceuserid);
            var toReturn = _achievedAchievementLogsService.GetUserAchievedAchievements(ProjectUser.UserId);
            if (toReturn != null)
            {
                toReturn = toReturn.Select(x => new AchievedAchievement { UserId = x.UserId, ProjectId = x.ProjectId, del = x.del, PointsAchieved = x.PointsAchieved, SourceUserId = x.SourceUserId, AchieveDate = x.AchieveDate, AchievedAchievementId = x.AchievedAchievementId, Achievement = x.Achievement, AchievementId = x.AchievementId });
                foreach (var item in toReturn)
                {
                    item.Achievement = _achievementService.GetAchievementDTO(item.AchievementId);
                }
                return toReturn.ToArray();
            }
            else
                return new AchievedAchievement[0];
        }

        public GameAssistantWeb.Models.Logs.AchievedAchievement AddAchievedAchievement(GameAssistantWeb.Models.Logs.AchievedAchievement AchievedAchievementToAdd)
        {
            string name = "";
            if (!GetData(out name) || AchievedAchievementToAdd == null)
                return null;

            var user = _accountService.GetUserByName(name);
            var AchievedAchievementAdded = _achievedAchievementLogsService.AddAchievedAchievement(AchievedAchievementToAdd, user.Id);

            AchievedAchievementAdded.Achievement = _achievementService.GetAchievementDTO(AchievedAchievementAdded.AchievementId);

            return AchievedAchievementAdded;
        }

        public bool DeleteAchievedAchievement(string projectid, string achievedeventid)
        {
            bool deleted = false;
            string name = "";
            int ProjectId, AchievedAchievementId = 0;

            if (!GetData(projectid, achievedeventid, out ProjectId, out AchievedAchievementId, out name))
                return deleted;

            var user = _accountService.GetUserByName(name);
            var AchievedAchievementToDelete = _achievedAchievementLogsService.GetAchievedAchievement(AchievedAchievementId);

            if (AchievedAchievementToDelete == null)
                return deleted;

            deleted = _achievedAchievementLogsService.DeleteAchievedAchievement(AchievedAchievementToDelete.AchievedAchievementId, user.Id);
            return deleted;
        }


        public static bool GetData(string projectid, out int ProjectId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || name == "")
            {
                ProjectId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad danych wejsciowych";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(string projectid, string sourceuserid, out int ProjectId, out int SourceUserId, out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (!Int32.TryParse(projectid, out ProjectId) || !Int32.TryParse(sourceuserid, out SourceUserId) || name == "")
            {
                ProjectId = SourceUserId = 0;
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad danych wejsciowych";
                return false;
            }
            else
                return true;

        }

        public static bool GetData(out string name)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            name = BasicAuthenticationHttpHeaderInjector.GetUserName(authHeader);

            if (name == "")
            {
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;
                response.StatusDescription = "Blad autoryzacji";
                return false;
            }
            else
                return true;

        }
    }
}
