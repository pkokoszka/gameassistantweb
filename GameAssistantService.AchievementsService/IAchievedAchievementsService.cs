﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GameAssistantService.AchievementsService
{

    [ServiceContract]
    public interface IAchievedAchievementsService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedAchievement[] GetAllAchievedAchievements();

        [OperationContract]
        [WebGet(UriTemplate = "/{projectid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedAchievement[] GetAllAchievedAchievementsByProjectId(string projectid);

        [OperationContract]
        [WebGet(UriTemplate = "/{projectid}/{sourceuserid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedAchievement[] GetUserAchievedAchievements(string projectid, string sourceuserid);


        [OperationContract]
        [WebInvoke(UriTemplate = "/", Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        AchievedAchievement AddAchievedAchievement(AchievedAchievement AchievedEventToAdd);



        [OperationContract]
        [WebInvoke(UriTemplate = "/{projectid}/{achievedeventid}", Method = "DELETE", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool DeleteAchievedAchievement(string projectid, string achievedeventid);
    }

}
