﻿using System;
namespace GameAssistantWeb.DataAccessLayer.Contracts
{
    public interface IUnitOfWork
    {
        void Commit();
        void Dispose();
        System.Data.Entity.DbSet<T> GetDbSet<T>() where T : class;
    }
}
