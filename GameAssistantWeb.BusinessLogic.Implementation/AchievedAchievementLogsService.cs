﻿using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class AchievedAchievementLogsService : IAchievedAchievementLogsService
    {
        private readonly IGameMechanicsService _gameMechanicsService;
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _uow;

        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<User> _userLogRepository;
        private readonly IRepository<Achievement> _achievementRepository;
        private readonly IRepository<AchievedAchievement> _achievedAchievementLogRepository;

        public AchievedAchievementLogsService(IUnitOfWork uow, IAccountService accountService, IGameMechanicsService gameMechanicsService, IRepository<User> userLogRepository, IRepository<Achievement> achievementRepository, IRepository<Project> projectRepository, IRepository<AchievedAchievement> achievedAchievementLogRepository)
        {
            _accountService = accountService;
            _gameMechanicsService = gameMechanicsService;
            _uow = uow;
            _achievementRepository = achievementRepository;
            _userLogRepository = userLogRepository;
            _projectRepository = projectRepository;
            _achievedAchievementLogRepository = achievedAchievementLogRepository;
        }



        public IEnumerable<AchievedAchievement> GetUserAchievedAchievements(int UserId, int ProjectId)
        {
            return _achievedAchievementLogRepository.Query().Where(x => x.del != true && x.UserId == UserId && x.ProjectId == ProjectId).ToList().
                Select(x => new AchievedAchievement { AchievedAchievementId = x.AchievedAchievementId, Achievement = GetAchievementDTO(x.Achievement), AchieveDate = x.AchieveDate, AchievementId = x.AchievementId, del = x.del, PointsAchieved = x.PointsAchieved, ProjectId = x.ProjectId, SourceUserId = x.SourceUserId, UserId = x.UserId }).AsEnumerable();
        }


        public IEnumerable<AchievedAchievement> GetUserAchievedAchievements(int UserId)
        {
            return _achievedAchievementLogRepository.Query().Where(x => x.del != true && x.UserId == UserId).ToList().
                Select(x => new AchievedAchievement { AchievedAchievementId = x.AchievedAchievementId, Achievement = GetAchievementDTO(x.Achievement), AchieveDate = x.AchieveDate, AchievementId = x.AchievementId, del = x.del, PointsAchieved = x.PointsAchieved, ProjectId = x.ProjectId, SourceUserId = x.SourceUserId, UserId = x.UserId }).AsEnumerable();
        }

        private Achievement GetAchievementDTO(Achievement achievement)
        {
            Achievement dto = new Achievement
            {
                AchievementId = achievement.AchievementId,
                AchievementType = achievement.AchievementType,
                CreateDate = achievement.CreateDate,
                del = achievement.del,
                EventsIteration = achievement.EventsIteration,
                ConnectedEventId = achievement.ConnectedEventId,
                PointsForAchieve = achievement.PointsForAchieve,
                ProjectId = achievement.ProjectId,
                Title = achievement.Title

            };
            return dto;
        }


        public IEnumerable<AchievedAchievement> GetAchievedAchievements()
        {
            return _achievedAchievementLogRepository.Query().Where(x => x.del != true).ToList().
                Select(x => new AchievedAchievement { AchievedAchievementId = x.AchievedAchievementId, Achievement = GetAchievementDTO(x.Achievement), AchieveDate = x.AchieveDate, AchievementId = x.AchievementId, del = x.del, PointsAchieved = x.PointsAchieved, ProjectId = x.ProjectId, SourceUserId = x.SourceUserId, UserId = x.UserId }).AsEnumerable();
        }

        public IEnumerable<AchievedAchievement> GetAchievedEvents(int ProjectId)
        {
            return _achievedAchievementLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).ToList().
                Select(x => new AchievedAchievement { AchievedAchievementId = x.AchievedAchievementId, Achievement = GetAchievementDTO(x.Achievement), AchieveDate = x.AchieveDate, AchievementId = x.AchievementId, del = x.del, PointsAchieved = x.PointsAchieved, ProjectId = x.ProjectId, SourceUserId = x.SourceUserId, UserId = x.UserId }).AsEnumerable();
        }

        public AchievedAchievement GetAchievedAchievement(int AchievedAchievementId)
        {
            var entity = _achievedAchievementLogRepository.Query().FirstOrDefault(x => x.del != true && x.AchievedAchievementId == AchievedAchievementId);
            entity.Achievement = GetAchievementDTO(entity.Achievement);
            return entity;
        }

        public bool DeleteAchievedAchievement(int AchievedAchievementId, int UserId)
        {
            var entity = _achievedAchievementLogRepository.GetEntityById(AchievedAchievementId);
            if (entity != null && entity.Project.OwnerId == UserId)
            {
                entity.del = true;
                _uow.Commit();
                return true;
            }
            return false;
        }

        public AchievedAchievement AddAchievedAchievement(AchievedAchievement AchievedAchievementToAdd, int UserId)
        {
            var user = _accountService.GetUserById(UserId);
            var project = _projectRepository.GetEntityById(AchievedAchievementToAdd.ProjectId);
            var achievement = _achievementRepository.GetEntityById(AchievedAchievementToAdd.AchievementId);

            if (project == null || achievement == null || project.OwnerId != user.Id)
                return new AchievedAchievement();

            AchievedAchievementToAdd.AchieveDate = DateTime.Now;
            AchievedAchievementToAdd.del = false;
            if (AchievedAchievementToAdd.UserId == 0)
                AchievedAchievementToAdd.UserId = _userLogRepository.Query().FirstOrDefault(x => x.SourceUserId == AchievedAchievementToAdd.SourceUserId && x.del != true).UserId;

            if (achievement.PointsForAchieve.HasValue)
                AchievedAchievementToAdd.PointsAchieved = achievement.PointsForAchieve.Value;
            else
                AchievedAchievementToAdd.PointsAchieved = 0;

            try
            {
                _achievedAchievementLogRepository.Add(AchievedAchievementToAdd);
                _uow.Commit();
                _gameMechanicsService.CalculateUserGameStatistics(AchievedAchievementToAdd.UserId);

                AchievedAchievementToAdd.Achievement = GetAchievementDTO(AchievedAchievementToAdd.Achievement);
                return AchievedAchievementToAdd;
            }
            catch (Exception)
            {
                return new AchievedAchievement();
            }
        }
    }
}
