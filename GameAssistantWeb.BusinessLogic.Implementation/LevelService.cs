﻿using AutoMapper;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class LevelService : ILevelService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Level> _levelRepository;


        public LevelService(IUnitOfWork uow, IRepository<Project> projectRepository, IRepository<Level> levelRepository)
        {
            _uow = uow;
            _projectRepository = projectRepository;
            _levelRepository = levelRepository;
        }

        public IEnumerable<ViewModels.Project.LevelViewModel> GetLevels(int ProjectId)
        {
            var entities = _levelRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).OrderBy(x => x.LowerThreshold);
            IEnumerable<LevelViewModel> levels = Mapper.Map<IEnumerable<Level>, IEnumerable<LevelViewModel>>(entities);

            return levels;
        }

        public void AddLevel(ViewModels.Project.LevelViewModel model, int UserId)
        {
            Level entityToAdd = Mapper.Map<LevelViewModel, Level>(model);
            entityToAdd.CreateDate = DateTime.Now;
            entityToAdd.del = false;

            _levelRepository.Add(entityToAdd);
            _uow.Commit();
        }

        public ViewModels.Project.LevelViewModel GetLevel(int ProjectId, int LevelId)
        {
            var entity = _levelRepository.Query().FirstOrDefault(x => x.del != true && x.ProjectId == ProjectId && x.LevelId == LevelId);
            LevelViewModel level = Mapper.Map<Level, LevelViewModel>(entity);

            return level;
        }

        public ViewModels.Project.LevelViewModel GetLevel(int LevelId)
        {
            var entity = _levelRepository.Query().FirstOrDefault(x => x.del != true && x.LevelId == LevelId);
            LevelViewModel level = Mapper.Map<Level, LevelViewModel>(entity);

            return level;
        }

        public void EditLevel(ViewModels.Project.LevelViewModel model, int UserId)
        {
            Level entityToEdit = _levelRepository.Query().FirstOrDefault(x => x.Project.OwnerId == UserId && x.del == false && x.LevelId == model.LevelId);
            if (entityToEdit != null)
            {
                entityToEdit.LevelTitle = model.LevelTitle;
                entityToEdit.LowerThreshold = model.LowerThreshold;
                entityToEdit.UpperThreshold = model.UpperThreshold;
                entityToEdit.UpperThresholdInf = model.UpperThresholdInf;

                _uow.Commit();
            }
        }

        public void DeleteLevel(int LevelId, int UserId)
        {
            Level entityToDelete = _levelRepository.Query().FirstOrDefault(x => x.LevelId == LevelId && x.Project.OwnerId == UserId && x.del == false);
            if (entityToDelete != null)
            {
                entityToDelete.del = true;
                _uow.Commit();
            }
        }


        public Level GetLevelDTO(int LevelId)
        {
            var entity = _levelRepository.Query().FirstOrDefault(x => x.del != true && x.LevelId == LevelId);
            Level entityToReturn = new Level { CreateDate = entity.CreateDate, del = entity.del, LevelId = entity.LevelId, LevelTitle = entity.LevelTitle, LowerThreshold = entity.LowerThreshold, ProjectId = entity.ProjectId, UpperThreshold = entity.UpperThreshold, UpperThresholdInf = entity.UpperThresholdInf };
            return entityToReturn;
        }
    }
}
