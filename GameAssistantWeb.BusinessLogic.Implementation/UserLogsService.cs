﻿using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class UserLogsService : IUserLogsService
    {
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _uow;

        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Level> _levelRepository;
        private readonly IRepository<User> _userLogRepository;
        private readonly IRepository<AchievedEvent> _achievedEventLogRepository;
        private readonly IRepository<AchievedAchievement> _achievedAchievementLogRepository;

        public UserLogsService(IUnitOfWork uow, IAccountService accountService, IRepository<Level> levelRepository, IRepository<User> userLogRepository, IRepository<Project> projectRepository, IRepository<AchievedEvent> achievedEventLogRepository, IRepository<AchievedAchievement> achievedAchievementLogRepository)
        {
            _accountService = accountService;
            _uow = uow;
            _userLogRepository = userLogRepository;
            _projectRepository = projectRepository;
            _achievedAchievementLogRepository = achievedAchievementLogRepository;
            _achievedEventLogRepository = achievedEventLogRepository;
            _levelRepository = levelRepository;
        }


        public IEnumerable<Models.Logs.User> GetUsers(int ProjectId)
        {
            IEnumerable<User> toReturn = _userLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId);
            return toReturn;
        }

        public Models.Logs.User GetUser(int UserId)
        {
            User user = _userLogRepository.GetEntityById(UserId);
            return user;
        }

        public bool DeleteUser(int UserId, int OwnerProjectId)
        {
            User user = _userLogRepository.GetEntityById(UserId);
            if (user != null && user.Project.OwnerId == OwnerProjectId)
            {
                user.del = true;
                _uow.Commit();
                return true;
            }
            return false;
        }


        public IEnumerable<ViewModels.Logs.UserActivity> GetUsersActivity(int ProjectId)
        {
            var userLogs = _userLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).
                Select(x => new UserActivity { CreateDate = (DateTime)x.CreateDate, Id = x.UserId, Title = "Add user: " + x.UserId, Type = "Add user" });

            var achievedEventLogs = _achievedEventLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).
                Select(x => new UserActivity { CreateDate = x.AchieveDate, Id = x.UserId, Title = "Achieve event: " + x.EventId + " " + x.Event.Title, Type = "Achieve event" });

            var achievedAchievementLogs = _achievedAchievementLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).
                Select(x => new UserActivity { CreateDate = x.AchieveDate, Id = x.UserId, Title = "Achieve achievement: " + x.AchievementId + " " + x.Achievement.Title, Type = "Achieve achievement" });


            var toReturn = userLogs.Concat(achievedAchievementLogs).Concat(achievedEventLogs).OrderByDescending(x=>x.CreateDate);
            return toReturn;
        }


        public User AddUser(User UserToAdd, int OwnerProjectId)
        {
            var user = _accountService.GetUserById(OwnerProjectId);
            var project = _projectRepository.GetEntityById(UserToAdd.ProjectId);

            if (project == null || project.OwnerId != user.Id)
                return new User();

            UserToAdd.CreateDate = DateTime.Now;
            UserToAdd.del = false;

            if (UserToAdd.LevelId == 0)
                UserToAdd.LevelId = _levelRepository.Query().OrderBy(x => x.LowerThreshold).
                    FirstOrDefault(x => x.ProjectId == UserToAdd.ProjectId && x.del != true).LevelId;

            try
            {
                _userLogRepository.Add(UserToAdd);
                _uow.Commit();

                return UserToAdd;
            }
            catch (Exception)
            {
                return new User();
            }
        }

        public User UpdateUser(User UserToUpdate, int OwnerProjectId)
        {
            var user = _accountService.GetUserById(OwnerProjectId);
            var project = _projectRepository.GetEntityById(UserToUpdate.ProjectId);

            if (project == null || project.OwnerId != user.Id)
                return new User();

            if(UserToUpdate.UserId == 0)
                return AddUser(UserToUpdate,OwnerProjectId);
            else
            {
                var entityToUpdate = _userLogRepository.GetEntityById(UserToUpdate.UserId);
                if(entityToUpdate == null)
                    return new User();

                entityToUpdate.AdditionalData = UserToUpdate.AdditionalData;
                entityToUpdate.UserName = UserToUpdate.UserName;
                entityToUpdate.SourceUserId = UserToUpdate.SourceUserId;
                _uow.Commit();

                return entityToUpdate;
            }
        }

        public User GetUser(int ProjectId, string SourceUserId)
        {
            User user = _userLogRepository.Query().FirstOrDefault(x => x.del != true && x.ProjectId == ProjectId && x.SourceUserId  == SourceUserId);
            return user;
        }

        public IEnumerable<User> GetUsers()
        {
            IEnumerable<User> toReturn = _userLogRepository.Query().Where(x => x.del != true);
            return toReturn;
        }
    }
}
