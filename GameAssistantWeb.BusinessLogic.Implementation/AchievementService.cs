﻿using AutoMapper;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class AchievementService : IAchievementService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Event> _eventRepository;
        private readonly IRepository<Achievement> _achievementRepository;


        public AchievementService(IUnitOfWork uow, IRepository<Project> projectRepository, IRepository<Event> eventRepository, IRepository<Achievement> achievementRepository)
        {
            _uow = uow;
            _projectRepository = projectRepository;
            _eventRepository = eventRepository;
            _achievementRepository = achievementRepository;
        }


        public IEnumerable<AchievementViewModel> GetAchievements(int ProjectId)
        {
            var entities = _achievementRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId);
            IEnumerable<AchievementViewModel> events = Mapper.Map<IEnumerable<Achievement>, IEnumerable<AchievementViewModel>>(entities);

            return events;
        }

        public AchievementViewModel CreateAchievementViewModel(int ProjectId)
        {
            AchievementViewModel model = new AchievementViewModel { ProjectId = ProjectId };
            model.Events = _eventRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).
                Select(y => new SelectListItem { Text = y.Title, Value = y.EventId.ToString() }).
                ToList();

            return model;
        }

        public AchievementViewModel GetAchievement(int AchievementId, int ProjectId)
        {
            var entity = _achievementRepository.Query().FirstOrDefault(x => x.del != true && x.AchievementId == AchievementId);
            AchievementViewModel achievement = Mapper.Map<Achievement, AchievementViewModel>(entity);
            achievement.Events = _eventRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).
                Select(y => new SelectListItem { Text = y.Title, Value = y.EventId.ToString() }).
                ToList();

            return achievement;
        }

        public AchievementViewModel GetAchievement(int AchievementId)
        {
            var entity = _achievementRepository.Query().FirstOrDefault(x => x.del != true && x.AchievementId == AchievementId);
            AchievementViewModel achievement = Mapper.Map<Achievement, AchievementViewModel>(entity);

            return achievement;
        }

        public void AddAchievement(AchievementViewModel model, int UserId)
        {
            Achievement entityToAdd = Mapper.Map<AchievementViewModel, Achievement>(model);
            entityToAdd.CreateDate = DateTime.Now;
            entityToAdd.del = false;


            _achievementRepository.Add(entityToAdd);
            _uow.Commit();
        }

        public void EditAchievement(AchievementViewModel model, int UserId)
        {
            Achievement entityToEdit = _achievementRepository.Query().FirstOrDefault(x => x.Project.OwnerId == UserId && x.del == false && x.AchievementId == model.AchievementId);
            if (entityToEdit != null)
            {
                entityToEdit.Title = model.Title;
                entityToEdit.EventsIteration = model.EventsIteration;
                entityToEdit.AchievementType = model.AchievementType;
                entityToEdit.ConnectedEventId = model.ConnectedEventId;
                entityToEdit.PointsForAchieve = model.PointsForAchieve;

                _uow.Commit();
            }
        }

        public void DeleteAchievement(int AchievementId, int UserId)
        {
            Achievement entityToEdit = _achievementRepository.Query().FirstOrDefault(x => x.Project.OwnerId == UserId && x.del == false && x.AchievementId == AchievementId);
            if (entityToEdit != null)
            {
                entityToEdit.del = true;

                _uow.Commit();
            }
        }



        public Achievement GetAchievementDTO(int AchievementId)
        {
            Achievement entity = _achievementRepository.Query().FirstOrDefault(x => x.del == false && x.AchievementId == AchievementId);
            if (entity != null)
            {
                var entityToReturn = new Achievement
                {
                    CreateDate = entity.CreateDate,
                    del = false,
                    AchievementId = entity.AchievementId,
                    AchievementType = entity.AchievementType,
                    EventsIteration = entity.EventsIteration,
                    ProjectId = entity.ProjectId,
                    ConnectedEventId = entity.ConnectedEventId,
                    PointsForAchieve = entity.PointsForAchieve,
                    Title = entity.Title
                };

                return entityToReturn;
            }
            else
            {
                return new Achievement();
            }
        }

    }
}
