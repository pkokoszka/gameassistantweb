﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRepository<ApplicationUser> _userRepository;

        public AccountService(IUnitOfWork uow, IRepository<ApplicationUser> userRepository)
        {
            _uow = uow;
            _userRepository = userRepository;
        }



        public ApplicationUser GetUserByName(string UserName)
        {
            return _userRepository.Query().FirstOrDefault(x => x.UserName == UserName && x.del != true);
        }


        public ApplicationUser GetUserById(int OwnerProjectId)
        {
            return _userRepository.Query().FirstOrDefault(x => x.Id == OwnerProjectId && x.del != true);
        }
    }
}
