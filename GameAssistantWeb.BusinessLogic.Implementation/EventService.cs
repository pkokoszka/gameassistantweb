﻿using AutoMapper;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class EventService : IEventService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Event> _eventRepository;


        public EventService(IUnitOfWork uow, IRepository<Project> projectRepository, IRepository<Event> eventRepository)
        {
            _uow = uow;
            _projectRepository = projectRepository;
            _eventRepository = eventRepository;
        }

        public IEnumerable<ViewModels.Project.EventViewModel> GetEvents(int ProjectId)
        {
            var entities = _eventRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId);
            IEnumerable<EventViewModel> events = Mapper.Map<IEnumerable<Event>, IEnumerable<EventViewModel>>(entities);

            return events;
        }

        public ViewModels.Project.EventViewModel GetEvent(int EventId)
        {
            var entity = _eventRepository.Query().FirstOrDefault(x => x.del != true && x.EventId == EventId);
            EventViewModel level = Mapper.Map<Event, EventViewModel>(entity);

            return level;
        }

        public void AddEvent(ViewModels.Project.EventViewModel model, int UserId)
        {
            Event entityToAdd = Mapper.Map<EventViewModel, Event>(model);
            entityToAdd.CreateDate = DateTime.Now;
            entityToAdd.del = false;

            _eventRepository.Add(entityToAdd);
            _uow.Commit();
        }

        public void EditEvent(ViewModels.Project.EventViewModel model, int UserId)
        {
            Event entityToEdit = _eventRepository.Query().FirstOrDefault(x => x.Project.OwnerId == UserId && x.del == false && x.EventId == model.EventId);
            if (entityToEdit != null)
            {
                entityToEdit.Title = model.Title;
                entityToEdit.Description = model.Description;
                entityToEdit.MultipleEvent = model.MultipleEvent;
                entityToEdit.PointsForAchieve = model.PointsForAchieve;

                _uow.Commit();
            }
        }

        public void DeleteEvent(int EventId, int UserId)
        {
            Event entityToEdit = _eventRepository.Query().FirstOrDefault(x => x.Project.OwnerId == UserId && x.del == false && x.EventId == EventId);
            if (entityToEdit != null)
            {
                entityToEdit.del = true;

                _uow.Commit();
            }
        }


        public Event GetEventDTO(int EventId)
        {
            var entity = _eventRepository.Query().FirstOrDefault(x => x.del != true && x.EventId == EventId);
            if (entity != null)
            {
                var entityToReturn = new Event
                {
                    CreateDate = entity.CreateDate,
                    del = false,
                    EventId = entity.EventId,
                    Description = entity.Description,
                    MultipleEvent = entity.MultipleEvent,
                    PointsForAchieve = entity.PointsForAchieve,
                    Title = entity.Title
                };

                return entityToReturn;
            }
            else
            {
                return new Event();
            }
        }
    }
}
