﻿using AutoMapper;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Project;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRepository<Project> _projectRepository;

        public ProjectService(IUnitOfWork uow, IRepository<Project> projectRepository)
        {
            _uow = uow;
            _projectRepository = projectRepository;
        }



        public IEnumerable<ProjectViewModel> GetProjetsByUserId(int UserId)
        {
            IEnumerable<Project> projectEntitiesList = _projectRepository.Query().Where(x => x.del != true && x.OwnerId == UserId);
            IEnumerable<ProjectViewModel> projectViewModelsList = Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectViewModel>>(projectEntitiesList);

            return projectViewModelsList;
        }

        public void AddProject(ViewModels.Project.ProjectViewModel model, int UserId)
        {
            Project entityToAdd = Mapper.Map<ProjectViewModel, Project>(model);
            entityToAdd.CreateDate = DateTime.Now;
            entityToAdd.OwnerId = UserId;
            entityToAdd.del = false;

            _projectRepository.Add(entityToAdd);
            _uow.Commit();
        }

        public ViewModels.Project.ProjectViewModel GetProjectById(int ProjectId, int UserId)
        {
            Project project = _projectRepository.Query().FirstOrDefault(x => x.del != true && x.ProjectId == ProjectId && x.OwnerId == UserId);
            ProjectViewModel projectViewModel = Mapper.Map<Project, ProjectViewModel>(project);

            return projectViewModel;
        }

        public void DeleteProject(int ProjectId, int UserId)
        {
            Project entityToDelete = _projectRepository.Query().FirstOrDefault(x => x.OwnerId == UserId && x.ProjectId == ProjectId && x.del == false);
            if (entityToDelete != null)
            {
                entityToDelete.del = true;
                _uow.Commit();
            }
        }


        public void EditProject(ProjectViewModel model, int UserId)
        {
            var entityToEdit = _projectRepository.Query().FirstOrDefault(x => x.OwnerId == UserId && x.ProjectId == model.ProjectId && x.del == false);
            if (entityToEdit != null)
            {
                entityToEdit.ProjectDescription = model.ProjectDescription;
                entityToEdit.ProjectName = model.ProjectName;
                entityToEdit.ProjectShortName = model.ProjectShortName;
                
                _uow.Commit();
            }
        }


        public ProjectViewModel GetProjectById(int ProjectId)
        {
            Project project = _projectRepository.Query().FirstOrDefault(x => x.del != true && x.ProjectId == ProjectId);
            ProjectViewModel projectViewModel = Mapper.Map<Project, ProjectViewModel>(project);

            return projectViewModel;
        }
    }
}
