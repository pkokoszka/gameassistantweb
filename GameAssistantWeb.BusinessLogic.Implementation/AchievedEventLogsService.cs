﻿using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;
using GameAssistantWeb.Repository.Contracts;
using GameAssistantWeb.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class AchievedEventLogsService : IAchievedEventLogsService
    {
        private readonly IAccountService _accountService;
        private readonly IGameMechanicsService _gameMechanicsService;

        private readonly IUnitOfWork _uow;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Event> _eventRepository;
        private readonly IRepository<User> _userLogRepository;
        private readonly IRepository<AchievedEvent> _achievedEventLogRepository;
        private readonly IRepository<AchievedAchievement> _achievedAchievementLogRepository;
        private readonly IRepository<Achievement> _achievementLogRepository;

        public AchievedEventLogsService(IUnitOfWork uow, IAccountService accountService, IGameMechanicsService gameMechanicsService, IRepository<AchievedAchievement> achievedAchievementLogRepository, IRepository<Achievement> achievementLogRepository, IRepository<User> userLogRepository, IRepository<Event> eventRepository, IRepository<Project> projectRepository, IRepository<AchievedEvent> achievedEventLogRepository)
        {
            _accountService = accountService;
            _uow = uow;
            _eventRepository = eventRepository;
            _userLogRepository = userLogRepository;
            _projectRepository = projectRepository;
            _gameMechanicsService = gameMechanicsService;
            _achievedAchievementLogRepository = achievedAchievementLogRepository;
            _achievementLogRepository = achievementLogRepository;
            _achievedEventLogRepository = achievedEventLogRepository;
        }



        public IEnumerable<AchievedEvent> GetUserAchievedEvents(int UserId, int ProjectId)
        {
            return _achievedEventLogRepository.Query().Where(x => x.del != true && x.UserId == UserId && x.ProjectId == ProjectId).ToList().
                Select(x => new AchievedEvent { AchieveDate = x.AchieveDate, SourceUserId = x.SourceUserId, AchievedEventId = x.AchievedEventId, del = x.del, PointsAchieved = x.PointsAchieved, EventId = x.EventId, ProjectId = x.ProjectId, UserId = x.UserId }); //as ICollection<AchievedEvent>;
        }


        public IEnumerable<AchievedEvent> GetUserAchievedEvents(int UserId)
        {
            return _achievedEventLogRepository.Query().Where(x => x.del != true && x.UserId == UserId).ToList().
                Select(x => new AchievedEvent { AchieveDate = x.AchieveDate, SourceUserId = x.SourceUserId , AchievedEventId = x.AchievedEventId, del = x.del, PointsAchieved = x.PointsAchieved, EventId = x.EventId, ProjectId = x.ProjectId, UserId = x.UserId}); //as ICollection<AchievedEvent>;
        }


        public bool DeleteAchievedEvent(int AchievedEventId, int UserId)
        {
            var entity = _achievedEventLogRepository.GetEntityById(AchievedEventId);
            if (entity != null && entity.Project.OwnerId == UserId)
            {
                entity.del = true;
                _uow.Commit();
                return true;
            }
            return false;
        }

        public IEnumerable<AchievedEvent> GetAchievedEvents(int ProjectId)
        {
            return _achievedEventLogRepository.Query().Where(x => x.del != true && x.ProjectId == ProjectId).ToList().Select(x => new AchievedEvent { AchieveDate = x.AchieveDate, SourceUserId = x.SourceUserId, AchievedEventId = x.AchievedEventId, del = x.del, PointsAchieved = x.PointsAchieved, EventId = x.EventId, ProjectId = x.ProjectId, UserId = x.UserId }); //as ICollection<AchievedEvent>;
        }

        public IEnumerable<AchievedEvent> GetAchievedEvents()
        {
            return _achievedEventLogRepository.Query().Where(x => x.del != true).ToList().Select(x => new AchievedEvent { AchieveDate = x.AchieveDate, SourceUserId = x.SourceUserId, AchievedEventId = x.AchievedEventId, del = x.del, PointsAchieved = x.PointsAchieved, EventId = x.EventId, ProjectId = x.ProjectId, UserId = x.UserId }); //as ICollection<AchievedEvent>;
        }

        public AchievedEvent AddAchievedEvent(AchievedEvent AchievedEventToAdd, int UserId)
        {
            var user = _accountService.GetUserById(UserId);
            var project = _projectRepository.GetEntityById(AchievedEventToAdd.ProjectId);

            if (project == null || project.OwnerId != user.Id)
                return new AchievedEvent();

            AchievedEventToAdd.AchieveDate = DateTime.Now;
            AchievedEventToAdd.del = false;
            if (AchievedEventToAdd.UserId == 0)
                AchievedEventToAdd.UserId = _userLogRepository.Query().FirstOrDefault(x => x.SourceUserId == AchievedEventToAdd.SourceUserId && x.del != true).UserId;

            if (_achievedEventLogRepository.Query().FirstOrDefault(x => x.EventId == AchievedEventToAdd.EventId && x.Event.MultipleEvent == false) != null)
                AchievedEventToAdd.PointsAchieved = 0;
            else
            {
                var entities = _achievedEventLogRepository.Query().Where(x => x.EventId == AchievedEventToAdd.EventId && x.Event.MultipleEvent == true);
                AchievedEventToAdd.PointsAchieved = _eventRepository.GetEntityById(AchievedEventToAdd.EventId).PointsForAchieve;
            }


            try
            {
                _achievedEventLogRepository.Add(AchievedEventToAdd);
                _uow.Commit();

                var achievementsToAdd = _achievementLogRepository.Query().Where(x => x.AchievementType == 1 && x.ConnectedEventId == AchievedEventToAdd.EventId && x.del != true);
                foreach (var item in achievementsToAdd)
                {
                    _achievedAchievementLogRepository.Add(new AchievedAchievement { AchieveDate = DateTime.Now, AchievementId = item.AchievementId, del = false, UserId = AchievedEventToAdd.UserId, ProjectId = AchievedEventToAdd.ProjectId, PointsAchieved = item.PointsForAchieve.HasValue ? item.PointsForAchieve.Value : 0 });
                }
                _uow.Commit();


                _gameMechanicsService.CalculateUserRepeatingAchievements(AchievedEventToAdd.UserId, AchievedEventToAdd.EventId);
                _gameMechanicsService.CalculateUserGameStatistics(AchievedEventToAdd.UserId);
                return AchievedEventToAdd;
            }
            catch (Exception)
            {
                return new AchievedEvent();
            }
        }
    }
}
