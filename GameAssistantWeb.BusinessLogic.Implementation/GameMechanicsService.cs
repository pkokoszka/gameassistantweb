﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.BusinessLogic.Contracts;
using GameAssistantWeb.Models.Identity;
using GameAssistantWeb.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;

namespace GameAssistantWeb.BusinessLogic.Implementation
{
    public class GameMechanicsService : IGameMechanicsService
    {
        private readonly IUnitOfWork _uow;

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Level> _levelRepository;
        private readonly IRepository<AchievedAchievement> _achievedAchievementRepository;
        private readonly IRepository<Achievement> _achievementRepository;
        private readonly IRepository<AchievedEvent> _achievedEventRepository;

        public GameMechanicsService(IUnitOfWork uow, IRepository<User> userRepository, IRepository<AchievedEvent> achievedEventRepository, IRepository<Achievement> achievementRepository, IRepository<Level> levelRepository, IRepository<AchievedAchievement> achievedAchievementRepository)
        {
            _uow = uow;
            _userRepository = userRepository;
            _levelRepository = levelRepository;
            _achievementRepository = achievementRepository;
            _achievedAchievementRepository = achievedAchievementRepository;
            _achievedEventRepository = achievedEventRepository;
        }



        public void CalculateUserPoints(int UserId)
        {
            var user = _userRepository.GetEntityById(UserId);
            var achievedEvents = _achievedEventRepository.Query().Where(x => x.UserId == user.UserId && x.del == false);
            var achievedAchievements = _achievedAchievementRepository.Query().Where(x => x.UserId == user.UserId && x.del == false);
            user.Points = 0;
            foreach (var item in achievedEvents)
            {
                user.Points = user.Points + item.PointsAchieved;
            }
            foreach (var item in achievedAchievements)
            {
                user.Points = user.Points + item.PointsAchieved;
            }

            _uow.Commit();
        }

        public void CalculateUserLevel(int UserId)
        {
            var user = _userRepository.GetEntityById(UserId);
            var level = _levelRepository.Query().FirstOrDefault(x => x.LowerThreshold <= user.Points && x.UpperThreshold > user.Points && x.ProjectId == user.ProjectId && x.del == false);

            if (level == null)
                level = _levelRepository.Query().FirstOrDefault(x => x.UpperThreshold <= user.Points && x.UpperThresholdInf == true && x.ProjectId == user.ProjectId && x.del == false);
            if (level == null)
                level = _levelRepository.Query().OrderBy(x => x.LowerThreshold).FirstOrDefault(x => x.del == false && x.ProjectId == user.ProjectId);

            user.LevelId = level.LevelId;

            _uow.Commit();
        }

        public void CalculateUserGameStatistics(int UserId)
        {
            CalculateUserPoints(UserId);
            CalculateUserLevel(UserId);
        }

        public void CalculateUserRepeatingAchievements(int UserId, int EventId)
        {
            var user = _userRepository.GetEntityById(UserId);
            IEnumerable<Achievement> achievementsConnectedToEvent = _achievementRepository.Query().Where(x => x.ConnectedEventId == EventId && x.del != true && x.AchievementType == 2);

            foreach (var item in achievementsConnectedToEvent)
            {
                int count = user.AchievedEvents.Where(x => x.EventId == EventId && x.del != true).Count();
                var achievedAchievements = user.AchievedAchievements.Where(x => x.del != true && x.AchievementId == item.AchievementId);
                foreach (var ach in achievedAchievements)
                {
                    count = count - ach.Achievement.EventsIteration.Value;
                }

                if (count == item.EventsIteration.Value)
                {
                    _achievedAchievementRepository.Add(new AchievedAchievement { AchievementId = item.AchievementId, AchieveDate = DateTime.Now, del = false, UserId = UserId, ProjectId = user.ProjectId, PointsAchieved = item.PointsForAchieve.HasValue ? item.PointsForAchieve.Value : 0 });
                    _uow.Commit();
                }
            }

        }
    }
}
