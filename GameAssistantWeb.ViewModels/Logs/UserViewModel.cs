﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.ViewModels.Logs
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        [Display(Name = "Source User ID")]
        public int SourceUserId { get; set; }
        public string ProjectUserId { get; set; }

        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Display(Name = "Points")]
        public int Points { get; set; }
        public string AdditionalData { get; set; }
        public DateTime CreateDate { get; set; }
        public bool del { get; set; }
        public int ProjectId { get; set; }
        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }
    }
}
