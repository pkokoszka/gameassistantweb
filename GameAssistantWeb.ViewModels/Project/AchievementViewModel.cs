﻿using GameAssistantWeb.Models.Logs;
using GameAssistantWeb.Models.Project;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GameAssistantWeb.ViewModels.Project
{
    public class AchievementViewModel
    {

        public int AchievementId { get; set; }
        [Required]
        [Display(Name = "Achievement type")]
        public int AchievementType { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        public GameAssistantWeb.Models.Enums.AchievementType Type
        {
            get
            {
                return (GameAssistantWeb.Models.Enums.AchievementType)AchievementType;
            }
            set {
                this.AchievementType = (int)value;
            }
        }



        public DateTime CreateDate { get; set; }
        public bool del { get; set; }

        [Display(Name = "Points (optionally)")]
        public int? PointsForAchieve { get; set; }

        [Display(Name = "Events iteration (for repeating event)")]
        public int? EventsIteration { get; set; }

        [Required]
        [Display(Name = "Connected event")]
        public int ConnectedEventId { get; set; }
        public virtual Event Event { get; set; }

        public List<SelectListItem> Events { get; set; }

        public int ProjectId { get; set; }
        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }

        public virtual ICollection<AchievedAchievement> AchievedAchievements { get; set; }
    }
}
