﻿using GameAssistantWeb.Models.Logs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GameAssistantWeb.ViewModels.Project
{
    public class EventViewModel
    {
        public int EventId { get; set; }

        public string EventNameId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }


        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }


        [Required]
        [Display(Name = "Points for achieve")]
        public int PointsForAchieve { get; set; }

        [Required]
        [Display(Name = "Multiple event")]
        public bool MultipleEvent { get; set; }

        public DateTime CreateDate { get; set; }

        public bool del { get; set; }

        public int ProjectId { get; set; }

        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }

        public virtual ICollection<AchievedEvent> AchievedEvents { get; set; }
    }
}
