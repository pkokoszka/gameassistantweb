﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.ViewModels.Project
{
    public class LevelViewModel
    {
        public int LevelId { get; set; }

        [Required]
        [Display(Name = "Level title")]
        public string LevelTitle { get; set; }

        [Required]
        [Display(Name = "Lower threshold")]
        public int LowerThreshold { get; set; }

        [Required]
        [Display(Name = "Upper threshold")]
        public int UpperThreshold { get; set; }

        [Required]
        [Display(Name = "Upper threshold to infinity")]
        public bool UpperThresholdInf { get; set; }

        public DateTime CreateDate { get; set; }

        public bool del { get; set; }

        public int ProjectId { get; set; }
        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }

    }
}
