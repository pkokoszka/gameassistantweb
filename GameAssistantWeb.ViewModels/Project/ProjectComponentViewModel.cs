﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.ViewModels.Project
{
    public class ProjectComponentViewModel
    {
        public int ProjectComponentId { get; set; }

        [Required]
        [Display(Name = "Component name")]
        public string ComponentName { get; set; }

        public DateTime CreateDate { get; set; }

        public int ProjectId { get; set; }
        public virtual GameAssistantWeb.Models.Project.Project Project { get; set; }
    }
}
