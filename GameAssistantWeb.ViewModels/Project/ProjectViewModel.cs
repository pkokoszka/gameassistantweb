﻿using GameAssistantWeb.Models.Project;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.ViewModels.Project
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }

        [Required]
        [Display(Name = "Project name")]
        public string ProjectName { get; set; }

        [Required]
        [Display(Name = "Project short name")]
        [StringLength(3, MinimumLength = 3)]
        [RegularExpression(@"(\S)+", ErrorMessage = " White Space is not allowed")]
        public string ProjectShortName { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "Description")]
        public string ProjectDescription { get; set; }

        public string TrimmedProjectDescription { get {
            if (this.ProjectDescription != null)
                if (this.ProjectDescription.Length > 40)
                    return new string(this.ProjectDescription.Take(40).ToArray())+"..";
                else
                    return new string(this.ProjectDescription.Take(40).ToArray());
            else
                return "";
        } }

        public bool del { get; set; }

        public int OwnerId { get; set; }

        public virtual GameAssistantWeb.Models.Identity.ApplicationUser Owner { get; set; }

        public virtual ICollection<ProjectComponent> Components { get; set; }
    }
}
