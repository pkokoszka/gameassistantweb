﻿using GameAssistantWeb.DataAccessLayer.Implementation;
using GameAssistantWeb.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Identity
{
    public class CustomRoleStore : RoleStore<CustomRole, int, CustomUserRole>
    {
        public CustomRoleStore(GameAssistantContext context)
            : base(context)
        {
        }
    } 
}
