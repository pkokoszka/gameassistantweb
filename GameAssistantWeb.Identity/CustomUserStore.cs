﻿using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.DataAccessLayer.Implementation;
using GameAssistantWeb.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAssistantWeb.Identity
{
    public class CustomUserStore : UserStore<ApplicationUser, CustomRole, int,
        CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomUserStore(IGameAssistantContext context)
            : base(context as DbContext)
        {
        }
    }
}
