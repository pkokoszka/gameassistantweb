﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using GameAssistantWeb.DataAccessLayer.Contracts;
using GameAssistantWeb.DataAccessLayer.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace GameAssistantWeb.ServiceApplication
{
    public class Global : System.Web.HttpApplication
    {
        IWindsorContainer _container;
        protected void Application_Start(object sender, EventArgs e)
        {
            //RegisterRoutes(RouteTable.Routes);
            _container = new WindsorContainer();

            _container.AddFacility<WcfFacility>()
                .Register
                (
                //Component.For<ILogger>().ImplementedBy<TextLogger>(),
                //Component.For<IRepository>().ImplementedBy<Repository>(),
                     Component.For<IService1>()
                              .ImplementedBy<Service1>()
                              .Named("Service1")

                //Component.For(typeof(IGameAssistantContext))
                //    .ImplementedBy(typeof(GameAssistantContext)).LifestylePerWebRequest(),
                //Component.For(typeof(IUnitOfWork))
                //    .ImplementedBy(typeof(UnitOfWork)).LifestylePerWebRequest()
                );
        }

        private void RegisterRoutes(RouteCollection routes)
        {
            routes.Add(new ServiceRoute("YourService",
                       new WebServiceHostFactory(), typeof(GameAssistantWeb.ServiceApplication.Service1)));
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }
    }
}